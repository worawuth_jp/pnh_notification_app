package com.example.pnhnotifier;

import android.os.AsyncTask;
import android.os.StrictMode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public class ApiServices  {
    private Retrofit retrofit;
    private String url;
    public ApiServices(){
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }
        Constant constant = new Constant();
        retrofit = new Retrofit.Builder()
                .baseUrl(constant.API_BASE_URL+"/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public ApiServices(String baseURL){
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);

        }
        retrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public Retrofit getRetrofit(){
        return this.retrofit;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public interface GetAllUserServices {
        @GET("/api/users?all=true")
        Call<ResponseBody> user();
    }

    public interface sendToUrl{
        @GET
        Call<ResponseBody> send(@Url String url);
    }

    public interface loginUser {
        @FormUrlEncoded
        @POST("/pn_api/login.php")
        Call<ResponseBody> login(@Field("uname") String uname,@Field("password") String password,@Field("sysname") String sysname);
    }

    public interface registerUser {
        @POST("/api/user")
        Call<ResponseBody> regis(@Body RequestBody body);
    }

    public interface getUser {
        @GET("/api/user")
        Call<ResponseBody> getUser(@Query("user_id") int userId,@Query("all") Boolean all);
    }

    public interface editUser {
        @PUT("/api/user")
        Call<ResponseBody> edit(@Body RequestBody body);
    }

    public interface GetApiLog {
        @GET("/api/log-api")
        Call<ResponseBody> getLog(@Query("user_id") int userId,@Query("api_id") int apiId,@Query("page_no") int pageNo, @Query("page_size") int pageSize);
    }

    public interface GetDisplayApi {
        @GET("/api/log-api/display")
        Call<ResponseBody> display(@Query("user_id") int userId);
    }

    public interface GetDisplayApiById {
        @GET("/api/log-api/display")
        Call<ResponseBody> display(@Query("api_id") int apiId,@Query("user_id") int userId);
    }

    public interface GetAllApi {
        @GET("/api/api-notification")
        Call<ResponseBody> getApi(@Query("user_id") int userId,@Query("all") Boolean all);
    }

    public interface GetApiById {
        @GET("/api/api-notification")
        Call<ResponseBody> getApi(@Query("user_id") int userId,@Query("all") Boolean all,@Query("api_id") int apiId);
    }

    public interface AddApi {
        @POST("/api/api-notification")
        Call<ResponseBody> addApi(@Body RequestBody body);
    }

    public interface EditApi {
        @PUT("/api/api-notification")
        Call<ResponseBody> edit(@Body RequestBody body);
    }

    public interface deleteApi {
        @DELETE("/api/log-api")
        Call<ResponseBody> delete(@Query("user_id") int userId,@Query("api_id") int apiId);
    }

    public interface deleteApiItem {
        @DELETE("/api/api-notification")
        Call<ResponseBody> delete(@Query("user_id") int userId,@Query("api_id") int apiId);
    }
}
