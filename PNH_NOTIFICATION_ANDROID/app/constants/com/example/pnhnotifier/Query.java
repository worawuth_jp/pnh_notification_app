package com.example.pnhnotifier;

public class Query {
    static Constant constant = new Constant();
    public static final String Find_API_BY_USERNAMEL = "SELECT *,DATE_FORMAT(created_at, '%d/%m/%Y %H:%i:%s') AS str_created_at FROM "+constant.DB_NAME+".api_notifications WHERE username=?";
    public static final String Find_API_BY_USERNAME_SORT_INTERVAL = "SELECT *,DATE_FORMAT(created_at, '%d/%m/%Y %H:%i:%s') AS str_created_at FROM "+constant.DB_NAME+".api_notifications WHERE username=? AND enable='Y' ORDER BY api_interval ASC";
    public static final String Find_API_BY_USERNAME_ID_SORT_INTERVAL = "SELECT *,DATE_FORMAT(created_at, '%d/%m/%Y %H:%i:%s') AS str_created_at FROM "+constant.DB_NAME+".api_notifications WHERE username=? AND api_id=? AND enable='Y' ORDER BY api_interval ASC";
    public static final String Find_API_BY_USERNAME_ID = "SELECT *,DATE_FORMAT(created_at, '%d/%m/%Y %H:%i:%s') AS str_created_at FROM "+constant.DB_NAME+".api_notifications WHERE username=? AND api_id=?";
    public static final String DELETE_API_BY_USERNAME_ID = String.format("DELETE FROM %s WHERE username=? AND api_id=?",constant.TABLE_API_NOTIFICATIONS);
    public static final String UPDATE_API_BY_ID = String.format("UPDATE %s SET api_key=?,api_name=?,api_interval=?,username=?,display_num=? WHERE api_id=?",constant.TABLE_API_NOTIFICATIONS);
    public static final String UPDATE_ENABLE_FLAG_BY_ID = String.format("UPDATE %s SET enable=? WHERE api_id=? AND username=?",Constant.TABLE_API_NOTIFICATIONS);
    public static final String Find_DISPLAY_NUM_BY_API_ID = "SELECT display_num FROM "+constant.DB_NAME+".api_notifications WHERE api_id=?";
}
