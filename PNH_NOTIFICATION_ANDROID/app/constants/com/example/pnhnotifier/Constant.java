package com.example.pnhnotifier;

import okhttp3.MediaType;

public class Constant {
    public static final String API_PATH = "/api";
    public static final String API_BASE_URL = "http://127.0.0.1";
    public static final String USERS_PATH = "/users";
    public static final String USER_PATH = "/user";


    public static final String LOGIN_PATH = "https://www.cuppattana.net:4433/";

    //Change Data About DB Connection This
    public static final String HOST_IP = "192.168.1.36";
    public static final String PORT = "3306";
    public static final String SYS_NAME = "mobileapp";
    public static final String DB_NAME = "mobileapp";
    public static final String USER_DB = "root"; //mobileapp
    public static final String PASS_DB = "12345678"; //tru@10789
    //END
    public static final String TABLE_API_NOTIFICATIONS = DB_NAME+".api_notifications";
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
}
