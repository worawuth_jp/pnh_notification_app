package com.example.pnhnotifier;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import retrofit2.Retrofit;

public class PagerAdapter extends FragmentStatePagerAdapter {
 int mNumOfTabs;

 public PagerAdapter(FragmentManager fm, int NumOfTabs) {
  super(fm);
  this.mNumOfTabs = NumOfTabs;
 }

 @Override
 public Fragment getItem(int position) {

  switch (position) {
   case 0:
    com.example.pnhnotifier.NotificationFragment tab1 = new com.example.pnhnotifier.NotificationFragment();
    return tab1;
   case 1:
    com.example.pnhnotifier.SettingFragment tab2 = new com.example.pnhnotifier.SettingFragment();
    return tab2;
   default:
    return null;
  }
 }

 @Override
 public int getCount() {
  return mNumOfTabs;
 }

 private void refresh(View v, int milliseconds, int userId, Retrofit retrofit){
  final Handler handler = new Handler();

  final Runnable runnable = new Runnable(){

   @Override
   public void run() {
   }
  };

  handler.postDelayed(runnable,milliseconds);
 }

}

