package com.example.pnhnotifier;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.support.v4.content.ContextCompat;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ActivityLogin extends AppCompatActivity {
    private TextView tvRegister;
    private EditText etLoginCID, etLoginPassword;
    private Button loginButton;

    private SQLiteDatabase db;
    private SQLiteOpenHelper openHelper;
    private Cursor cursor;

    private Constant constant = new Constant();

    public static List<Integer> apiId = new ArrayList<Integer>();
    public static List<String> apiTime = new ArrayList<String>();
    public static HashMap<Integer,String> api = new HashMap<Integer,String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getSharedPreferences("store",MODE_PRIVATE);
        @SuppressLint("WrongConstant") SharedPreferences sh = this.getSharedPreferences("store", Context.MODE_APPEND);
        int userId = sh.getInt("userId", 0);
        if(userId != 0){
            startActivity(new Intent(ActivityLogin.this, MainActivity.class));
        }
        setContentView(R.layout.activity_login);

        openHelper = new DatabaseHelper(this);
        db = openHelper.getReadableDatabase();
        tvRegister = findViewById(R.id.tvRegister);
        etLoginCID = findViewById(R.id.etLogCID);
        etLoginPassword = findViewById(R.id.etLoginPassword);
        loginButton = findViewById(R.id.btnLogin);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cid = etLoginCID.getText().toString().trim();
                String password = etLoginPassword.getText().toString().trim();
                if (cid.isEmpty() || password.isEmpty()) {
                    Toast.makeText(ActivityLogin.this, "โปรดใส่เลขบัตรประชาชนและรหัสผ่านของคุณเพื่อเข้าสู่ระบบ", Toast.LENGTH_SHORT).show();
                } else {
                    ApiServices apiServices = new ApiServices(constant.LOGIN_PATH);
                    Retrofit retrofit = apiServices.getRetrofit();

                    //JSONObject jsonObject = new JSONObject();
                    //                        jsonObject.put("id", cid);
//                        jsonObject.put("password", password);
//                        RequestBody body = RequestBody.create(constant.JSON, jsonObject.toString());

                    ApiServices.loginUser services = retrofit.create(ApiServices.loginUser.class);
                    Call<ResponseBody> call = services.login(cid,password,constant.SYS_NAME);

                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            try {
                                if(response.code() == 400){
                                    JSONObject jsonObject1 = new JSONObject(response.errorBody().string());
                                    int statusCode = jsonObject1.getInt("statusCode");
                                    String statusMessage = jsonObject1.getString("statusMessage");
                                    System.out.println("ERROR "+statusCode+" MSG: "+statusMessage);
                                    Toast.makeText(ActivityLogin.this, statusMessage, Toast.LENGTH_SHORT).show();
                                }else{
                                    String responseData = response.body().string();
                                    System.out.println(response);
                                    JSONArray jsonArray = new JSONArray(responseData);

                                    if(response.code() == 200){
                                        if(jsonArray.length() > 0){
                                            JSONObject user = jsonArray.getJSONObject(0);
                                            @SuppressLint("CommitPrefEdits") SharedPreferences.Editor myEdit = sharedPreferences.edit();
                                            myEdit.putInt("userId",user.getInt("user_id"));
                                            myEdit.putString("cardId",user.getString("user_card_id"));
                                            myEdit.putString("username",user.getString("user_username"));
                                            myEdit.putString("phone",user.getString("user_phone"));
                                            myEdit.putString("password",user.getString("password"));
                                            myEdit.commit();

                                            Toast.makeText(ActivityLogin.this, "เข้าสู่ระบบสำเร็จ", Toast.LENGTH_SHORT).show();
                                            startService();
                                            startActivity(new Intent(ActivityLogin.this, MainActivity.class));
                                        }else{
                                            System.out.println("ERROR MSG: Username or Password Wrong");
                                            Toast.makeText(ActivityLogin.this, "เข้าสู่ระบบไม่สำเร็จ", Toast.LENGTH_SHORT).show();
                                        }
                                    }

//                                    int statusCode = jsonObject1.getInt("statusCode");
//                                    String statusMessage = jsonObject1.getString("statusMessage");
//                                    if(statusCode == 200 && statusMessage.equals("SUCCESS")){
////                                        JSONObject data = jsonObject1.getJSONObject("data");
////                                        JSONObject user = data.getJSONObject("user");
//
//                                        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor myEdit = sharedPreferences.edit();
//                                        myEdit.putInt("userId",user.getInt("user_id"));
//                                        myEdit.putString("cardId",user.getString("user_card_id"));
//                                        myEdit.putString("username",user.getString("user_username"));
//                                        myEdit.putString("phone",user.getString("user_phone"));
//                                        myEdit.putString("password",user.getString("password"));
//                                        myEdit.commit();
//
//                                        Toast.makeText(ActivityLogin.this, data.getString("msg"), Toast.LENGTH_SHORT).show();
//                                        startService();
//                                        startActivity(new Intent(ActivityLogin.this, MainActivity.class));
//                                    }
                                    System.out.println(responseData);
                                }

                            } catch (Exception e) {
                                System.out.println("FAIL111"+e.getMessage());
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            //
                            System.out.println("FAIL");
                            t.printStackTrace();

                        }
                    });


                }
            }
        });
        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityLogin.this, RegisterActivity.class));
                finish();
            }
        });
    }

    public void startService() {
        String cidservice = etLoginCID.getText().toString();
        Intent serviceIntent = new Intent(this, com.example.pnhnotifier.ForegroundService.class);
        serviceIntent.putExtra("cidservice", cidservice);
        startService(serviceIntent);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    @Override
    public void onBackPressed() {
        //do your action
        finish();

    }
}