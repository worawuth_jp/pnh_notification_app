package com.example.pnhnotifier;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class RegisterActivity extends AppCompatActivity {
    private Button registerBtn, gotoLoginBtn;
    private Dialog dialog;
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private EditText regName, regPhone, regCID, regPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerBtn = findViewById(R.id.btnRegLogin);
        gotoLoginBtn = findViewById(R.id.btnGotoLogin);
        regName = findViewById(R.id.etRegName);
        regPhone = findViewById(R.id.etRegPhone);
        regCID = findViewById(R.id.etRegCID);
        regPassword = findViewById(R.id.etRegPassword);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fname = regName.getText().toString().trim();
                String fPhone = regPhone.getText().toString().trim();
                String fCID = regCID.getText().toString().trim();
                String fPassword = regPassword.getText().toString().trim();
                if (fname.isEmpty() || fPassword.isEmpty() || fCID.isEmpty() || fPhone.isEmpty()) {
                    Toast.makeText(RegisterActivity.this, "กรุณากรอกรายละเอียดให้ครบถ้วน", Toast.LENGTH_SHORT).show();
                } else {
                    insertData();
                    dialog.show();
                    Toast.makeText(RegisterActivity.this, "ยืนยันการลงทะเบียน", Toast.LENGTH_SHORT).show();

                }
            }
        });

        gotoLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, ActivityLogin.class));
                finish();
            }
        });
    }

    public void insertData() {
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_dialog_register);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.custom_dialog_background));
        }
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false); //Optional
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //Setting the animations to dialog

        Button Okay = dialog.findViewById(R.id.btn_okay);
        Button Cancel = dialog.findViewById(R.id.btn_cancel);

        Okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("user_username", regName.getText().toString().trim());
                    jsonObject.put("user_phone", regPhone.getText().toString().trim());
                    jsonObject.put("user_card_id", regCID.getText().toString().trim());
                    jsonObject.put("user_password", regPassword.getText().toString().trim());

                    Constant constant = new Constant();
                    ApiServices apiServices = new ApiServices();
                    Retrofit retrofit = apiServices.getRetrofit();

                    RequestBody body = RequestBody.create(constant.JSON, jsonObject.toString());
                    ApiServices.registerUser services = retrofit.create(ApiServices.registerUser.class);
                    Call<ResponseBody> call = services.regis(body);

                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            try {
                                if (response.code() == 400) {
                                    JSONObject jsonObject1 = new JSONObject(response.errorBody().string());
                                    int statusCode = jsonObject1.getInt("statusCode");
                                    String statusMessage = jsonObject1.getString("statusMessage");
                                    System.out.println("ERROR " + statusCode + " MSG: " + statusMessage);
                                    Toast.makeText(RegisterActivity.this, statusMessage, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                } else {
                                    String responseData = response.body().string();
                                    JSONObject jsonObject1 = new JSONObject(responseData);
                                    int statusCode = jsonObject1.getInt("statusCode");
                                    String statusMessage = jsonObject1.getString("statusMessage");
                                    if (statusCode == 200 && statusMessage.equals("SUCCESS")) {
                                        JSONObject data = jsonObject1.getJSONObject("data");
                                        Toast.makeText(RegisterActivity.this, "โปรดยืนยันการลงทะเบียน", Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(RegisterActivity.this, ActivityLogin.class));
                                        finish();
                                        dialog.dismiss();
                                    }
                                    System.out.println(responseData);
                                }

                            } catch (Exception e) {
                                System.out.println("FAIL111" + e.getMessage());
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            //
                            System.out.println("FAIL");
                            t.printStackTrace();

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(RegisterActivity.this, "ยกเลิกการลงทะเบียน", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show(); // Showing the dialog here
            }
        });
    }
}
