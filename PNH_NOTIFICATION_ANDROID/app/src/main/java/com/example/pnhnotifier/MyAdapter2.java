package com.example.pnhnotifier;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MyAdapter2 extends RecyclerView.Adapter<MyAdapter2.MyViewHolder> {
    private Dialog dialog;
    List<Api> data;
    Context context;
    SharedPreferences sp;

    public MyAdapter2(Context ct, List<Api> data){
        context = (Context) ct;
        sp = ct.getSharedPreferences("store", Context.MODE_PRIVATE);
        this.data = data;
    }
    @NonNull
    @Override
    public MyAdapter2.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.lst_template,viewGroup,false);
        return new MyViewHolder(view).linkAdapter(this);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter2.MyViewHolder myViewHolder, @SuppressLint("RecyclerView") int i) {
        try {
            System.out.println(data.get(i).api_name);
            myViewHolder.lblId.setText(Integer.toString(data.get(i).api_id));
            myViewHolder.lblAPIkey.setText(data.get(i).api_key);
            myViewHolder.lblAPIName.setText(data.get(i).api_name);
            myViewHolder.lblfeq_time.setText("" + (new DecimalFormat("##.##").format(data.get(i).api_interval / 60)));


            Log.d("SET",(data.get(i).enable.equals("Y") ? "TRUE":"FALSE"));
            myViewHolder.enableApiSwitch.setChecked((data.get(i).enable.equals("Y")));
            myViewHolder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Constant constant = new Constant();
                    LoadingDialog loadingDialog = new LoadingDialog(context);
                    loadingDialog.start();

                    int userId = 0,apiId=0;
                    String username;
                    userId = sp.getInt("userId",0);
                    username = sp.getString("cardId","");
                    apiId = data.get(i).api_id;

                    ConnectDBServices connectDBServices = new ConnectDBServices();
                    try {
                        Query query = new Query();
                        Connection con = connectDBServices.getCon();
                        String sql = query.DELETE_API_BY_USERNAME_ID;
                        PreparedStatement ps = con.prepareStatement(sql);
                        ps.setString(1,username);
                        ps.setInt(2,apiId);
                        int rowCount = ps.executeUpdate();
                        if(rowCount > 0){
                            myViewHolder.adapter.data.remove(i);
                            myViewHolder.adapter.notifyItemRemoved(i);
                        }
                        loadingDialog.dismiss();

                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                        loadingDialog.dismiss();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                        loadingDialog.dismiss();
                    }
                }
            });
            myViewHolder.edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context,EditApiActivity.class);
                    intent.putExtra("apiId",data.get(i).api_id);
                    context.startActivity(intent);
                }
            });

            myViewHolder.enableApiSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    update(i,b);
                    dialog.show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    public void update(int i,boolean value){
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom_dialog_edit);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawable(context.getDrawable(R.drawable.custom_dialog_background));
        }
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false); //Optional
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //Setting the animations to dialog

        Button Okay = dialog.findViewById(R.id.btn_okay);
        Button Cancel = dialog.findViewById(R.id.btn_cancel);

        Okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int apiId = data.get(i).api_id;
                String username = sp.getString("cardId","");
                String enable = value ? "Y":"N";
                String sql = Query.UPDATE_ENABLE_FLAG_BY_ID;
                ConnectDBServices connectDBServices = new ConnectDBServices();
                try {
                    Connection con = connectDBServices.getCon();
                    PreparedStatement ps = con.prepareStatement(sql);
                    ps.setString(1,enable);
                    ps.setInt(2,apiId);
                    ps.setString(3,username);
                    ps.executeUpdate();
                    ps.close();
                    con.close();
                    dialog.dismiss();

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

            }
        });

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView lblId,lblAPIkey,lblAPIName,lblfeq_time;
        ImageView edit,delete;
        MyAdapter2 adapter;
        Switch enableApiSwitch;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            lblId=itemView.findViewById(R.id.lblId);
            lblAPIkey=itemView.findViewById(R.id.lblAPIkey);
            lblAPIName=itemView.findViewById(R.id.lblAPIName);
            lblfeq_time=itemView.findViewById(R.id.lblfeq_time);
            enableApiSwitch = itemView.findViewById(R.id.enableApiSwitch);

            edit=itemView.findViewById(R.id.editImageView);
            delete=itemView.findViewById(R.id.deleteImageView);
        }

        public MyViewHolder linkAdapter(MyAdapter2 adapter2){
            this.adapter = adapter2;
            return this;
        }
    }
}
