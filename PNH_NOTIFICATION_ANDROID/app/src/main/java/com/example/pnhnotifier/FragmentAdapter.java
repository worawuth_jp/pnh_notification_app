package com.example.pnhnotifier;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import java.util.ArrayList;

public class FragmentAdapter extends FragmentPagerAdapter {

 Context context;
 ArrayList<Fragment> fragments;

 public FragmentAdapter(FragmentManager fm, Context context, ArrayList<Fragment> fragments) {
  super(fm);
  this.context = context;
  this.fragments = fragments;
 }

 @Override
 public int getCount() {
  return fragments.size();
 }


 @Override
 public Fragment getItem(int i) {
  return fragments.get(i);
 }

}