package com.example.pnhnotifier;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class API_Required extends SQLiteOpenHelper {
 private static final String tablename = "PNH_API_Required"; // tablename
 private static final String API = "API"; // column name
 private static final String id = "ID"; // auto generated ID column
 private static final String ApiName = "ApiName"; // column name
 private static final String feq_time = "feq_time";
 private static final String databasename = "dbAPIs"; // Dtabasename
 private static final int versioncode = 1; //versioncode of the database

 public API_Required(Context context) {
  super(context, databasename, null, versioncode);

 }

 @Override
 public void onCreate(SQLiteDatabase database) {
  String query;
  query = "CREATE TABLE IF NOT EXISTS " + tablename + "(" + id + " integer primary key, "
   + API + " text, " + ApiName + " text, " + feq_time + "  text )";
  database.execSQL(query);
 }

 @Override
 public void onUpgrade(SQLiteDatabase database, int version_old,
                       int current_version) {
  String query;
  query = "DROP TABLE IF EXISTS " + tablename;
  database.execSQL(query);
  onCreate(database);
 }

 public ArrayList<HashMap<String, String>> getAPIs() {

  ArrayList<HashMap<String, String>> APIList = new ArrayList<HashMap<String, String>>();
  SQLiteDatabase database = this.getWritableDatabase();
  Cursor cursor = database.rawQuery("SELECT * FROM " + tablename, null);
  if (cursor.moveToFirst()) {
   do {
    HashMap<String, String> map = new HashMap<String, String>();
    map.put("id", cursor.getString(0));
    map.put("API", cursor.getString(1));
    map.put("ApiName", cursor.getString(2));
    map.put("feq_time", cursor.getString(3));
    APIList.add(map);
   } while (cursor.moveToNext());
  }

  cursor.close();
  database.close();

// return contact list
  return APIList;
 }

 public boolean addAPI(String APIapi, String APIname, String APIfeq_time) {
  try {
   SQLiteDatabase db = this.getWritableDatabase();
   ContentValues cv = new ContentValues();
   cv.put(API, APIapi);
   cv.put(ApiName, APIname);
   cv.put(feq_time, APIfeq_time);
   db.insert(tablename, null, cv);
   db.close();

   return true;
  } catch (Exception ex) {
   return false;
  }

 }
}