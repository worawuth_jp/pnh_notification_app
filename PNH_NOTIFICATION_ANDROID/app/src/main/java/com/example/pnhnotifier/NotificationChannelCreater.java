package com.example.pnhnotifier;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.List;

public class NotificationChannelCreater extends Application {
    public static final String CHANNEL_ID = "Notification_PNH";

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(CHANNEL_ID, "Foreground Service Channel", NotificationManager.IMPORTANCE_HIGH);

            SharedPreferences sharedPreferences = this.getSharedPreferences("setting",MODE_PRIVATE);
            boolean isVibrate = sharedPreferences.getBoolean("is_vibrate",true);
            boolean isNotification = sharedPreferences.getBoolean("is_notification",true);
            Uri notificationRing = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            serviceChannel.enableVibration(isVibrate);
//            serviceChannel.setSound(null,null);
            serviceChannel.enableLights(true);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);

        }
    }
}
