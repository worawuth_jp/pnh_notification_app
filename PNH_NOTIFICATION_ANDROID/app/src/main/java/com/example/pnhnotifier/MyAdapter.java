package com.example.pnhnotifier;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    List<LogApi> data;
    Context context;


    public MyAdapter(Context ct, List<LogApi> data){
        context = (Context) ct;
        this.data = data;
    }
    @NonNull
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.my_row,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder myViewHolder, @SuppressLint("RecyclerView") int i) {
        try {
            System.out.println(data.get(i).api_name);
            myViewHolder.apiNameTxt.setText(data.get(i).api_name);
            myViewHolder.responseTxt.setText(data.get(i).api_message);
            myViewHolder.textTime.setText(data.get(i).api_timestamp);
            myViewHolder.titleText.setText(""+data.get(i).api_title);
            myViewHolder.deleteApiBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    myViewHolder.loadingDialog.start();
                    String username;
                    @SuppressLint("WrongConstant") SharedPreferences sp = context.getSharedPreferences("store",Context.MODE_APPEND);
                    int apiId = data.get(i).api_id;
                    username = sp.getString("cardId","");

                    ApiNotificationDB apiNotificationDB = new ApiNotificationDB(context);
                    apiNotificationDB.deleteLogApi(apiId,username);
                    data.remove(i);
                    myViewHolder.loadingDialog.dismiss();

                }
            });
            myViewHolder.detailApiBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context,NotificationDetailActivity.class);
                    intent.putExtra("apiId",data.get(i).api_id);
                    context.startActivity(intent);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView apiNameTxt,responseTxt,textTime,titleText;
        Button detailApiBtn,deleteApiBtn;
        LoadingDialog loadingDialog;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            titleText=itemView.findViewById(R.id.api_title);
            apiNameTxt = itemView.findViewById(R.id.api_name);
            responseTxt = itemView.findViewById(R.id.response);
            detailApiBtn = itemView.findViewById(R.id.detailApi);
            deleteApiBtn = itemView.findViewById(R.id.deleteApi);
            loadingDialog = new LoadingDialog(itemView.getContext());
            textTime = itemView.findViewById(R.id.textTime);
        }
    }
}
