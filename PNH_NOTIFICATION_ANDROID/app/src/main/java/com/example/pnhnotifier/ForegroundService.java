package com.example.pnhnotifier;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import static com.example.pnhnotifier.NotificationChannelCreater.CHANNEL_ID;

public class ForegroundService extends Service {

 @Override
 public void onCreate() {

  super.onCreate();
 }

 @Override
 public int onStartCommand(Intent intent, int flags, int startId) {
  String cidservice = intent.getStringExtra("cidservice");

  Intent notificationIntent = new Intent(this, com.example.pnhnotifier.ActivityLogin.class);
  PendingIntent pendingIntent = PendingIntent.getActivity(this,0,notificationIntent,0);

  NotificationCompat.Builder notification=new NotificationCompat.Builder(this,CHANNEL_ID)
   .setContentTitle("แจ้งเตือน!")
   .setContentText(cidservice + '"'  +  "เข้าสู่ระบบสำเร็จแล้ว" +'"')
   .setSmallIcon(com.example.pnhnotifier.R.drawable.ic_notifications)
   .setContentIntent(pendingIntent);

  startForeground(1,notification.build());
  return START_NOT_STICKY;
 }

 @Nullable
 @Override
 public IBinder onBind(Intent intent) {
  return null;
 }

 @Override
 public void onDestroy() {
  super.onDestroy();
 }
}
