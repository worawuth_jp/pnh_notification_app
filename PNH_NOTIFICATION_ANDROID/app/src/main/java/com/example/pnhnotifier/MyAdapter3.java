package com.example.pnhnotifier;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MyAdapter3 extends RecyclerView.Adapter<MyAdapter3.MyViewHolder> {

    List<LogApi> data;
    Context context;

    public MyAdapter3(Context ct, List<LogApi> data){
        context = (Context) ct;
        this.data = data;
    }
    @NonNull
    @Override
    public MyAdapter3.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.my_row_details,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter3.MyViewHolder myViewHolder, @SuppressLint("RecyclerView") int i) {
        System.out.println(data.get(i).api_name);
        myViewHolder.apiNameTxt.setText(data.get(i).api_name);
        myViewHolder.responseTxt.setText(data.get(i).api_message);
        myViewHolder.textTime.setText(data.get(i).api_timestamp);
        myViewHolder.titleText.setText(""+data.get(i).api_title);

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView apiNameTxt,responseTxt,textTime,titleText;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            titleText=itemView.findViewById(R.id.api_title);
            apiNameTxt = itemView.findViewById(R.id.api_name);
            responseTxt = itemView.findViewById(R.id.response);

            textTime = itemView.findViewById(R.id.textTime);
        }
    }
}
