package com.example.pnhnotifier;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.json.JSONObject;

import java.security.Provider;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ConsumeMessageFromApi extends Service {
    Date date;
    Query query = new Query();
    List<Timer> timer1 = new ArrayList<>();
    List<Api> apiList = new ArrayList<>();
    ConnectDBServices connectDBServices;
    SharedPreferences sharedPreferences;
    ApiNotificationDB apiNotificationDB;
    Context context = getApplication();
    private static final String TAG = "ConsumeMessageService";

    public int countApiListOnDB() throws SQLException, ClassNotFoundException {
        Connection con = connectDBServices.getCon();
        String sql = query.Find_API_BY_USERNAME_SORT_INTERVAL;
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setString(1, sharedPreferences.getString("cardId", ""));
        ResultSet resultSet = ps.executeQuery();
        int count = 0;
        while (resultSet.next()) {
            count++;
        }
        resultSet.close();
        ps.close();
        con.close();
        return count;
    }

    public void setApiList() throws SQLException, ClassNotFoundException {
        System.out.println("SET API LIST : " + sharedPreferences.getString("cardId", ""));
        Connection con = connectDBServices.getCon();
        String sql = query.Find_API_BY_USERNAME_SORT_INTERVAL;
        PreparedStatement ps = null;
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, sharedPreferences.getString("cardId", ""));
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                int api_id = resultSet.getInt("api_id");
                String api_key = resultSet.getString("api_key");
                String api_name = resultSet.getString("api_name");
                int api_interval = (int) resultSet.getDouble("api_interval");
                String username = resultSet.getString("username");
                int display_num = resultSet.getInt("display_num");
                String enable = resultSet.getString("enable");
                String created_at = resultSet.getString("str_created_at");
                Api api = new Api(api_id, api_key, api_name, api_interval, username, display_num, enable, created_at);
                System.out.println(api);
                apiList.add(api);
                setReceiptInterval(api_id);
            }
            resultSet.close();
            ps.close();
            con.close();
        } catch (Exception throwables) {
            throwables.printStackTrace();
        }

    }

    public void process() throws SQLException, ClassNotFoundException {
        System.out.println("START PROCESS......");
        Log.d("LOG-WRITER", "START PROCESS........");
        // Query get Api of this username and sort by interval then loop and call setReceiptInterval
        setApiList();
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    //Log.d("NLOG",apiList.size() + "  "+countApiListOnDB());
                    if (apiList.size() == 0 || countApiListOnDB() != apiList.size()) {
                        stopInterval();
                        apiList = new ArrayList<>();
                        System.out.println("SET API LIST : " + sharedPreferences.getString("cardId", ""));
                        Connection con = connectDBServices.getCon();
                        String sql = query.Find_API_BY_USERNAME_SORT_INTERVAL;
                        PreparedStatement ps = null;
                        try {
                            ps = con.prepareStatement(sql);
                            ps.setString(1, sharedPreferences.getString("cardId", ""));
                            ResultSet resultSet = ps.executeQuery();
                            while (resultSet.next()) {
                                int api_id = resultSet.getInt("api_id");
                                String api_key = resultSet.getString("api_key");
                                String api_name = resultSet.getString("api_name");
                                int api_interval = (int) resultSet.getDouble("api_interval");
                                String username = resultSet.getString("username");
                                int display_num = resultSet.getInt("display_num");
                                String enable = resultSet.getString("enable");
                                String created_at = resultSet.getString("str_created_at");
                                Api api = new Api(api_id, api_key, api_name, api_interval, username, display_num, enable, created_at);
                                apiList.add(api);
                            }
                            resultSet.close();
                            ps.close();
                            con.close();
                        } catch (Exception throwables) {
                            throwables.printStackTrace();
                        }
                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 1000);
    }

    public void setReceiptInterval(int apiId) {
        System.out.println("INTERVAL............");
        Timer t1 = new Timer();
        timer1.add(t1);
        timer1.get(timer1.size() - 1).scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                Date date1;
                try {
                    Connection con = connectDBServices.getCon();
                    String sql = query.Find_API_BY_USERNAME_ID_SORT_INTERVAL;
                    PreparedStatement ps = con.prepareStatement(sql);
                    ps.setString(1, sharedPreferences.getString("cardId", ""));
                    ps.setInt(2, apiId);
                    ResultSet resultSet = ps.executeQuery();
                    int interval = 0;
                    String apiKey = "";
                    String sDate1 = "";
                    String apiName = "";
                    while (resultSet.next()) {
                        interval = resultSet.getInt("api_interval");
                        apiKey = resultSet.getString("api_key");
                        sDate1 = resultSet.getString("str_created_at");
                        apiName = resultSet.getString("api_name");
                        System.out.println("VALUE : " + interval + " " + apiKey + " " + sDate1);
                    }
                    final String api_name = apiName;
                    resultSet.close();
                    ps.close();
                    con.close();

                    date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(sDate1);
                    date = new Date();
                    long diffTime = ((long) date.getTime() / 1000) - ((long) date1.getTime() / 1000);
                    if (diffTime % interval == 0 && date.getTime() > date1.getTime()) {
                        Pattern pattern = Pattern.compile("([^=&]*=[^=&]*)", Pattern.CASE_INSENSITIVE);
                        Matcher matcher = pattern.matcher(apiKey);
                        boolean matchFound = matcher.find();
                        if(matchFound){
                            apiKey += "&userid=";
                        }else{
                            apiKey += "?userid=";
                        }
                        apiKey += sharedPreferences.getString("cardId", "");
                        System.out.println("WriteLog");
                        Log.d("LOG-WRITER", "Get Message Listener AT " + formatter.format(date) + " Send " + apiKey);
                        ApiServices apiServices = new ApiServices();
                        Retrofit retrofit = apiServices.getRetrofit();


                        ApiServices.sendToUrl services = retrofit.create(ApiServices.sendToUrl.class);
                        Call<ResponseBody> call = services.send(apiKey);

                        call.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                try {
                                    if (response.code() == 200) {
                                        String responseData = response.body().string();
                                        Log.d("S-System","response data : "+responseData);
                                        JSONObject data = new JSONObject(responseData);
                                        String title = data.getString("title");
                                        String message = data.getString("message");
                                        String username = sharedPreferences.getString("cardId", "");

                                        ApiNotificationDB apiNotificationDB = new ApiNotificationDB(context);
                                        boolean status = apiNotificationDB.addLogGetMessage(apiId,api_name,title,message,username);
                                        if(status){
                                            Log.d("S-System","Write Message Success");
                                        }else{
                                            Log.e("S-System","Write Message Failed");
                                        }

                                    }
                                } catch (Exception e) {
                                    System.out.println("FAIL : " + e.getMessage());
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                //
                                System.out.println("FAIL");
                                t.printStackTrace();

                            }
                        });


                        //apiNotificationDB.addLogGetMessage(apiId,);
                    }
                } catch (ParseException | SQLException | ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }, 0, 1000);
    }

    public void stopInterval() {
        System.out.println("STOP ALL INTERVAL");
        int len = timer1.size();
        while (len > 0) {
            len = len - 1;
            timer1.get(len).cancel();
            timer1.remove(len);
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            sharedPreferences = this.getSharedPreferences("store", Context.MODE_PRIVATE);
            context = this;
            apiNotificationDB = new ApiNotificationDB(context);
            connectDBServices = new ConnectDBServices();
            process();
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
