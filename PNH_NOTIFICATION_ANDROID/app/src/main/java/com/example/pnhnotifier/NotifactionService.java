package com.example.pnhnotifier;

import static com.example.pnhnotifier.NotificationChannelCreater.CHANNEL_ID;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NotifactionService extends Service {
    List<Integer> apiId = ActivityLogin.apiId;
    List<String> apiTime = ActivityLogin.apiTime;
    HashMap<Integer, String> api = ActivityLogin.api;
    SharedPreferences sp, sh;

    @Override
    public void onCreate() {

        super.onCreate();
    }

    @SuppressLint("WrongConstant")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Set init starter value temp
        //System.out.println("START NOTIFICATION .......");
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getApplicationContext());

        sp = this.getSharedPreferences("store", Context.MODE_APPEND);
        sh = this.getSharedPreferences("setting", Context.MODE_APPEND);
        int userId = sp.getInt("userId", 0);

        ApiNotificationDB apiNotificationDB = new ApiNotificationDB(this);
        String username = sp.getString("cardId", "");
        List<LogApi> displayApi = new ArrayList<>();
        displayApi = apiNotificationDB.getDisplayLog(username);

        //System.out.println("START INIT API");
        if (api.size() != displayApi.size()) {
            api = new HashMap<Integer, String>();
            for (int i = 0; i < displayApi.size(); i++) {
                LogApi json = displayApi.get(i);
                api.put(json.api_id, json.api_timestamp);
            }
            ActivityLogin.api = api;
        }

        ShowNoti(userId, pendingIntent, getApplicationContext(), notificationManagerCompat);

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void ShowNoti(int userId, PendingIntent pendingIntent, Context ct, NotificationManagerCompat n) {
        Intent boardcastIntent = new Intent(this, MainActivity.class);
        PendingIntent actionContent = PendingIntent.getBroadcast(this, 0, boardcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        ApiNotificationDB apiNotificationDB = new ApiNotificationDB(this);
        String username = sp.getString("cardId", "");
        List<LogApi> displayApi = new ArrayList<>();
        displayApi = apiNotificationDB.getDisplayLog(username);
        List<LogApi> apiLists = new ArrayList<>();
        apiLists.clear();
        apiLists = displayApi;

        long[] vibrate = new long[]{100, 500, 1000, 500, 100};
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
//                            int notificationRing = NotificationCompat.DEFAULT_SOUND;
        if (!sh.getBoolean("is_notification", true)) {
            System.out.println("NOTIFICATION SOUND : False");
            soundUri = null;
        }
        if (!sh.getBoolean("is_vibrate", true)) {
            System.out.println("VIBRATE SOUND : False");
            vibrate = new long[]{};
        }

        int i = 0;
        for (LogApi json : apiLists) {
            String timestamp = api.get(json.api_id);
            if (!timestamp.equals(json.api_timestamp)) {
                Log.d("W-NOTIFICATION","NOTIFICATION!!!!!");
                NotificationCompat.Builder notification = new NotificationCompat.Builder(ct, CHANNEL_ID)
                        .setContentTitle(json.api_name)
                        .setContentText("เรื่อง " + json.api_title)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setSmallIcon(R.drawable.ic_notifications)
                        .setSound(soundUri)
                        .setVibrate(vibrate)
                        .setLights(Color.YELLOW, 200, 200)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

                int id = i + 2;
                //startForeground(id,notification.build());
                n.notify(id, notification.build());
                //System.out.println("NOTIFICATION ID : " + id);
            }

            //System.out.println("CHANGE VALUE.....");
            api.put(json.api_id, json.api_timestamp);
            //System.out.println(api);

        }

    }

    private void refresh(int milliseconds, int userId, PendingIntent
            pendingIntent, Context ct, NotificationManagerCompat n) {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                //System.out.println("Refresh()");
                ShowNoti(userId, pendingIntent, ct, n);
                handler.postDelayed(this, milliseconds);
            }
        };

        handler.postDelayed(runnable, 1000);
    }
}