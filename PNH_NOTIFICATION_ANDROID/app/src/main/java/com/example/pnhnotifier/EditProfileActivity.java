package com.example.pnhnotifier;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EditProfileActivity extends AppCompatActivity {

    EditText editRegUsername,editRegPhone,editRegCardId,editRegPassword;
    Button editButton,backButton;
    private Dialog dialog;
    SharedPreferences sp;
    int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                userId= 0;
            } else {
                userId= extras.getInt("userId");
            }
        } else {
            userId= (int) savedInstanceState.getSerializable("userId");
        }

        sp = getApplication().getSharedPreferences("store", Context.MODE_PRIVATE);

        editRegUsername = findViewById(R.id.editRegName);
        editRegPhone = findViewById(R.id.editRegPhone);
        editRegCardId = findViewById(R.id.editRegCID);
        editRegPassword = findViewById(R.id.editRegPassword);

        editRegUsername.setText(sp.getString("username",""));
        editRegPhone.setText(sp.getString("phone",""));
        editRegCardId.setText(sp.getString("cardId",""));
        editRegPassword.setText(sp.getString("password",""));

        editButton = findViewById(R.id.editButton);
        backButton = findViewById(R.id.backButton);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editData();
                dialog.show();
            }
        });

    }

    public void editData(){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_dialog_edit);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.custom_dialog_background));
        }
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false); //Optional
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //Setting the animations to dialog

        Button Okay = dialog.findViewById(R.id.btn_okay);
        Button Cancel = dialog.findViewById(R.id.btn_cancel);

        Okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("user_id", Integer.toString(userId).trim());
                    jsonObject.put("user_username", editRegUsername.getText().toString().trim());
                    jsonObject.put("user_phone", editRegPhone.getText().toString().trim());
                    jsonObject.put("user_card_id", editRegCardId.getText().toString().trim());
                    jsonObject.put("user_password", editRegPassword.getText().toString().trim());
                    jsonObject.put("enable", "Y");

                    Constant constant = new Constant();
                    ApiServices apiServices = new ApiServices();
                    Retrofit retrofit = apiServices.getRetrofit();

                    RequestBody body = RequestBody.create(constant.JSON, jsonObject.toString());
                    ApiServices.editUser services = retrofit.create(ApiServices.editUser.class);
                    Call<ResponseBody> call = services.edit(body);

                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            try {
                                if (response.code() == 400) {
                                    JSONObject jsonObject1 = new JSONObject(response.errorBody().string());
                                    int statusCode = jsonObject1.getInt("statusCode");
                                    String statusMessage = jsonObject1.getString("statusMessage");
                                    System.out.println("ERROR " + statusCode + " MSG: " + statusMessage);
                                    Toast.makeText(EditProfileActivity.this, statusMessage, Toast.LENGTH_SHORT).show();
                                    dialog.dismiss();
                                } else {
                                    String responseData = response.body().string();
                                    JSONObject jsonObject1 = new JSONObject(responseData);
                                    int statusCode = jsonObject1.getInt("statusCode");
                                    String statusMessage = jsonObject1.getString("statusMessage");
                                    if (statusCode == 200 && statusMessage.equals("SUCCESS")) {
                                        JSONObject data = jsonObject1.getJSONObject("data");
                                        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor myEdit = sp.edit();
                                        myEdit.putInt("userId",userId);
                                        myEdit.putString("cardId",editRegCardId.getText().toString().trim());
                                        myEdit.putString("username",editRegUsername.getText().toString().trim());
                                        myEdit.putString("phone",editRegPhone.getText().toString().trim());
                                        myEdit.putString("password",editRegPassword.getText().toString().trim());
                                        myEdit.commit();
                                        finish();
                                        dialog.dismiss();
                                    }
                                    System.out.println(responseData);
                                }

                            } catch (Exception e) {
                                System.out.println("FAIL_1111" + e.getMessage());
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            //
                            System.out.println("FAIL");
                            t.printStackTrace();

                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}