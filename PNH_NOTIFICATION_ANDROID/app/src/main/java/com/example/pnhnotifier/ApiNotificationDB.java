package com.example.pnhnotifier;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ApiNotificationDB extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="mobile.db";
    private static final String TABLE_NAME="log_api";
    private static final String api_id="api_id";
    private static final String api_name="api_name";
    private static final String api_title="api_title";
    private static final String api_message="api_message";
    private static final String api_timestamp="api_timestamp";
    private static final String username="username";
    public ApiNotificationDB(Context context) {
        super(context, DATABASE_NAME, null, 1);
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
//        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(String.format("create table %s (%s INTEGER , %s text, %s text,%s TEXT,%s DATETIME DEFAULT CURRENT_TIMESTAMP,%s TEXT);CREATE INDEX idx_log_api_id \n" +
                "ON %s (%s);",TABLE_NAME,api_id,api_name,api_title,api_message,api_timestamp,username,TABLE_NAME,api_id)
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void deleteLogApi(int apiId,String userName){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, api_id + "=" + apiId+" AND "+username+ "="+ userName,null);
    }

    public void showAll(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<LogApi> array_list = new ArrayList<LogApi>();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME+" GROUP BY api_id",null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            int api_id = res.getInt(res.getColumnIndex("api_id"));
            String api_name = res.getString(res.getColumnIndex("api_name"));
            String api_title = res.getString(res.getColumnIndex("api_title"));
            String api_message = res.getString(res.getColumnIndex("api_message"));
            String api_timestamp = res.getString(res.getColumnIndex("api_timestamp"));
            array_list.add(new LogApi(api_id,api_name,api_title,api_message,api_timestamp));
            res.moveToNext();
            Log.d("SHOW-DATA",String.format("Data : %d %s %s %s",api_id,api_title,api_message,api_timestamp));
        }
        res.close();
    }

    public boolean addLogGetMessage(int api_id, String api_name, String api_title, String api_message, String username){
        SQLiteDatabase db = this.getWritableDatabase();
        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        System.out.println(date);
        ContentValues contentValues = new ContentValues();
        contentValues.put("api_id", api_id);
        contentValues.put("api_name", api_name);
        contentValues.put("api_title", api_title);
        contentValues.put("api_message", api_message);
        contentValues.put("username", username);
        contentValues.put("api_timestamp",date);
        db.replace(TABLE_NAME, null, contentValues);
        return true;
    }

    public ArrayList<LogApi> getDisplayLog(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<LogApi> array_list = new ArrayList<LogApi>();
        Cursor res = db.rawQuery("select api_id,api_name,api_title,api_message,api_timestamp,strftime('%d/%m/%Y %H:%M:%S',api_timestamp) AS str_timestamp from " + TABLE_NAME + " WHERE username = '"+username+"' GROUP BY api_id ORDER BY api_timestamp DESC", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            int api_id = res.getInt(res.getColumnIndex("api_id"));
            String api_name = res.getString(res.getColumnIndex("api_name"));
            String api_title = res.getString(res.getColumnIndex("api_title"));
            String api_message = res.getString(res.getColumnIndex("api_message"));
            String api_timestamp = res.getString(res.getColumnIndex("str_timestamp"));
            array_list.add(new LogApi(api_id,api_name,api_title,api_message,api_timestamp));
            res.moveToNext();
            Log.d("S-System",String.format("Data : %d %s %s %s",api_id,api_title,api_message,api_timestamp));
        }
        res.close();
        return array_list;
    }

    public ArrayList<LogApi> getDisplayLog(int pageSize,String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<LogApi> array_list = new ArrayList<LogApi>();
        Cursor res = db.rawQuery("select api_id,api_name,api_title,api_message,api_timestamp,strftime('%d/%m/%Y %H:%M:%S',api_timestamp) AS str_timestamp from " + TABLE_NAME +" WHERE username="+username+" GROUP BY api_id ORDER BY api_timestamp DESC LIMIT "+pageSize, null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            int api_id = res.getInt(res.getColumnIndex("api_id"));
            String api_name = res.getString(res.getColumnIndex("api_name"));
            String api_title = res.getString(res.getColumnIndex("api_title"));
            String api_message = res.getString(res.getColumnIndex("api_message"));
            String api_timestamp = res.getString(res.getColumnIndex("str_timestamp"));
            array_list.add(new LogApi(api_id,api_name,api_title,api_message,api_timestamp));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public ArrayList<LogApi> getDetailLog(int pageSize,int api_id) throws SQLException, ClassNotFoundException {
        ConnectDBServices connectDBServices = new ConnectDBServices();
        Connection con = connectDBServices.getCon();
        PreparedStatement ps = con.prepareStatement(Query.Find_DISPLAY_NUM_BY_API_ID);
        ps.setInt(1,api_id);
        ResultSet rs = ps.executeQuery();
        int D_NUM = pageSize;
        while (rs.next()){
            D_NUM = rs.getInt("display_num");
        }
        rs.close();
        ps.close();
        con.close();
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<LogApi> array_list = new ArrayList<LogApi>();
        Cursor res = db.rawQuery("select *,strftime('%d/%m/%Y %H:%M:%S',api_timestamp) AS str_timestamp from " + TABLE_NAME + " WHERE api_id="+api_id+" ORDER BY api_timestamp DESC LIMIT "+D_NUM, null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            String api_title = res.getString(res.getColumnIndex("api_title"));
            String api_name = res.getString(res.getColumnIndex("api_name"));
            String api_message = res.getString(res.getColumnIndex("api_message"));
            String api_timestamp = res.getString(res.getColumnIndex("api_timestamp"));
            array_list.add(new LogApi(api_id,api_name,api_title,api_message,api_timestamp));
            res.moveToNext();
        }

        res.close();
        return array_list;
    }

}
