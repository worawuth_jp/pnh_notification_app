package com.example.pnhnotifier;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {
 @Override
 protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  setContentView(R.layout.activity_main);
  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
  toolbar.setTitle("PNH notifier MAIN MENU");
  setSupportActionBar(toolbar);
  TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);

  tabLayout.addTab(tabLayout.newTab().setText("ข้อความ").setIcon(R.drawable.ic_baseline_message_24));
  tabLayout.addTab(tabLayout.newTab().setText("ตั้งค่า").setIcon(R.drawable.ic_baseline_settings_24));
  tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

  final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
  final com.example.pnhnotifier.PagerAdapter adapter = new com.example.pnhnotifier.PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
  viewPager.setAdapter(adapter);
  viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
  tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
   @Override
   public void onTabSelected(TabLayout.Tab tab) {
    viewPager.setCurrentItem(tab.getPosition());
   }

   @Override
   public void onTabUnselected(TabLayout.Tab tab) {

   }

   @Override
   public void onTabReselected(TabLayout.Tab tab) {

   }
  });
 }
}



   /*if (savedInstanceState == null) {
    getSupportFragmentManager()
     .beginTransaction()
     .add(R.id.contentFragmentMain, new MainFragment())
     .commit();
   }*/