package com.example.pnhnotifier;

import android.app.PendingIntent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import top.defaults.colorpicker.ColorPickerPopup;

import android.support.v7.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v4.app.Fragment;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Map;


public class SettingFragment extends Fragment {
    private Spinner prefLocalCacheSize;
    private EditText prefPreviewLineCount;

    private Switch prefMsgLowEnableSound;
    private TextView prefMsgLowRingtone_value;
    private View prefMsgLowRingtone_container;
    private Switch prefMsgLowRepeatSound;
    private Switch prefMsgLowEnableLED;
    private View prefMsgLowLedColor_container;
    private ImageView prefMsgLowLedColor_value;
    private int mDefaultColor;
    private Switch prefMsgLowEnableVibrations;
    private Switch prefMsgLowForceVolume;
    private SeekBar prefMsgLowVolume;
    private ImageView prefMsgLowVolumeTest;
    private TextView tvfrequency,name,location,designation;
    private Button api_registerBtn;


    private Switch prefMsgNormEnableSound;
    private TextView prefMsgNormRingtone_value;
    private View prefMsgNormRingtone_container;
    private Switch prefMsgNormRepeatSound;
    private Switch prefMsgNormEnableLED;
    private View prefMsgNormLedColor_container;
    private ImageView prefMsgNormLedColor_value;
    private Switch prefMsgNormEnableVibrations;
    private Switch prefMsgNormForceVolume;
    private SeekBar prefMsgNormVolume;
    private ImageView prefMsgNormVolumeTest;

    private Switch prefMsgHighEnableSound;
    private TextView prefMsgHighRingtone_value;
    private View prefMsgHighRingtone_container;
    private Switch prefMsgHighRepeatSound;
    private Switch prefMsgHighEnableLED;
    private View prefMsgHighLedColor_container;
    private ImageView prefMsgHighLedColor_value;
    private Switch prefMsgHighEnableVibrations;
    private Switch prefMsgHighForceVolume;
    private SeekBar prefMsgHighVolume;
    private ImageView prefMsgHighVolumeTest;

    private Button signout;
    private Button prefBtnExport;
    private ImageView editBtn;
    SharedPreferences sharedPreferences;
    SharedPreferences sp;

    private int musicPickerSwitch = -1;

    private MediaPlayer[] mPlayers = new MediaPlayer[3];
    boolean isNotification;
    boolean isVibate;

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View settings = inflater.inflate(R.layout.fragment_settings, container, false);
        sharedPreferences = this.getActivity().getSharedPreferences("setting", Context.MODE_PRIVATE);
        sp = this.getActivity().getSharedPreferences("store",Context.MODE_APPEND);
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor spEdit = sp.edit();
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor myEdit = sharedPreferences.edit();

        name = settings.findViewById(R.id.name);
        location = settings.findViewById(R.id.location);
        designation = settings.findViewById(R.id.designation);

        loadData();

        prefMsgLowEnableSound = settings.findViewById(R.id.prefMsgLowEnableSound);
        prefMsgLowEnableSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                System.out.println("NOTIFICATION SWITCH "+isChecked);
                System.out.println("VALUE IN setting "+sharedPreferences.getBoolean("is_notification",false));
                myEdit.putBoolean("is_notification",isChecked);
                myEdit.commit();
            }
        });

        prefMsgLowEnableVibrations = settings.findViewById(R.id.prefMsgLowEnableVibrations);
        prefMsgLowEnableVibrations.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                System.out.println("NOTIFICATION SWITCH "+isChecked);
                System.out.println("VALUE IN setting "+sharedPreferences.getBoolean("is_vibrate",false));
                myEdit.putBoolean("is_vibrate",isChecked);
                myEdit.commit();
            }
        });

        editBtn = settings.findViewById(R.id.edit);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(inflater.getContext(),EditProfileActivity.class);
                int userId = sp.getInt("userId",0);
                if(userId != 0){
                    intent.putExtra("userId",userId);
                }
                startActivity(intent);
            }
        });

        api_registerBtn = settings.findViewById(R.id.api_registerBtn);
        signout = settings.findViewById(R.id.signout);

        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spEdit.remove("userId");
                spEdit.remove("cardId");
                spEdit.remove("username");
                spEdit.remove("phone");
                spEdit.remove("password");
                spEdit.apply();
                Intent stopSelf = new Intent(getActivity(), NotifactionService.class);
                PendingIntent pStopSelf = PendingIntent.getService(getActivity(), 0, stopSelf,PendingIntent.FLAG_CANCEL_CURRENT);
                startActivity(new Intent(inflater.getContext(), ActivityLogin.class));
            }
        });


        api_registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), api_register.class));
            }
        });

        loadSetting();
        onResume();
        return settings;
    }

    @Override
    public void onResume() {
        loadData();
        super.onResume();
    }


    private void loadSetting() {
        isNotification = sharedPreferences.getBoolean("is_notification", true);
        isVibate = sharedPreferences.getBoolean("is_vibrate", true);
        prefMsgLowEnableSound.setChecked(isNotification);
        prefMsgLowEnableVibrations.setChecked(isVibate);
    }

    private void loadData(){
        name.setText(sp.getString("username",""));
        location.setText(sp.getString("phone",""));
        designation.setText(sp.getString("cardId",""));
    }


}
