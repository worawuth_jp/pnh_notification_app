package com.example.pnhnotifier;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import android.support.v7.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class api_register extends AppCompatActivity {

    Button btnAdd;
    TextView tvBack;
    EditText txtAPIkey, txtAPIname, txtAPIfreqtime,txtTitleTemp,txtContentTemp,displayNumTxt;
    ImageView edit,delete;
    API_Required API_Required;
    ListView lstApiRegister;
    RecyclerView listApiRV;
    private Constant constant = new Constant();
    int userId;
    LoadingDialog loadingDialog;

    SharedPreferences sharedPreferences ;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.api_register);
        sharedPreferences = getSharedPreferences("store", Context.MODE_APPEND);
        userId = sharedPreferences.getInt("userId", 0);

        loadingDialog = new LoadingDialog(api_register.this);

        System.out.println("USERID : "+userId);

        btnAdd = findViewById(R.id.btnAdd);

        txtAPIkey = findViewById(R.id.txtAPIkey);
        txtAPIname = findViewById(R.id.txtAPIname);
        txtAPIfreqtime = findViewById(R.id.txtAPIfreqtime);
        displayNumTxt = findViewById(R.id.display_num);
//        txtTitleTemp = findViewById(R.id.txtTitleTemp);
//        txtContentTemp = findViewById(R.id.txtContentTemp);
        tvBack = findViewById(R.id.tvBack);
//        lstApiRegister = findViewById(R.id.lstApiRegister);
        listApiRV = findViewById(R.id.listApiRV);

        clear();
        API_Required = new API_Required(getApplicationContext());
        loadingDialog.start();
        setAPIs();

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(api_register.this, MainActivity.class));
            }
        });


    }

    @Override
    protected void onResume() {
        setAPIs();
        super.onResume();
    }

    public void clear() {
        txtAPIkey.setText("");
        txtAPIname.setText("");
        txtAPIfreqtime.setText("");
        displayNumTxt.setText("30");
        //txtAPITitle.setText("");
    }

    public void addAPI(View v) {
        try {
            if (TextUtils.isEmpty(txtAPIkey.getText().toString()) ||
                    TextUtils.isEmpty(txtAPIname.getText().toString()) ||
                    TextUtils.isEmpty(txtAPIfreqtime.getText().toString())) {
                Toast.makeText(api_register.this, "โปรดใส่ API key, ชื่อของข้อมูล & ความถี่เพื่อบันทึก", Toast.LENGTH_SHORT).show();
                return;
            }

            String username = sharedPreferences.getString("cardId","");
            ConnectDBServices connectDBServices = new ConnectDBServices();
            Connection connection = connectDBServices.getCon();
            String sql = String.format("INSERT INTO %s(api_key,api_name,api_interval,username,display_num) VALUES(?,?,?,?,?)",constant.TABLE_API_NOTIFICATIONS);
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1,txtAPIkey.getText().toString().trim());
            ps.setString(2,txtAPIname.getText().toString().trim());
            ps.setString(3,txtAPIfreqtime.getText().toString().trim());
            ps.setString(4,username);
            ps.setInt(5,Integer.parseInt(displayNumTxt.getText().toString().trim()));
            int rowCount = ps.executeUpdate();
            ps.close();
            connection.close();
            if(rowCount > 0){
                clear();
                Toast.makeText(api_register.this, "API saved successfully", Toast.LENGTH_SHORT).show();
                setAPIs();
            } else
                Toast.makeText(api_register.this, "Cannot save API", Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            Toast.makeText(api_register.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void setAPIs() {
        try {
            List<HashMap<String, String>> store = new ArrayList<>();
            edit = findViewById(R.id.editImageView);
            delete = findViewById(R.id.deleteImageView);

            String username = sharedPreferences.getString("cardId","");
            ConnectDBServices connectDBServices = new ConnectDBServices();
            Connection con = connectDBServices.getCon();
            Query query = new Query();
            String sql = query.Find_API_BY_USERNAMEL;
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1,username);
            ResultSet rs = ps.executeQuery();
            List<Api> apiLists = new ArrayList<>();
            while (rs.next()){
                int api_id = rs.getInt("api_id");
                String api_key = rs.getString("api_key");
                String api_name = rs.getString("api_name");
                int api_interval = rs.getInt("api_interval");
                String user = rs.getString("username");
                int display_num = rs.getInt("display_num");
                String enable = rs.getString("enable");
                String created_at = rs.getString("str_created_at");
                Api apiObj = new Api(api_id,api_key,api_name,api_interval,user,display_num,enable,created_at);
                Log.d("DATA-OUT",api_id+" "+api_key+" "+api_name);
                apiLists.add(apiObj);
            }
            rs.close();
            ps.close();
            con.close();

            System.out.println("DATA SIZE MY ADAPTER2 : " + apiLists.size());


            MyAdapter2 myAdapter = new MyAdapter2(api_register.this, apiLists);
            listApiRV.setAdapter(myAdapter);
            listApiRV.setLayoutManager(new LinearLayoutManager(api_register.this));
            loadingDialog.dismiss();

//            ApiServices.GetAllApi services = retrofit.create(ApiServices.GetAllApi.class);
//            Call<ResponseBody> call = services.getApi(userId, false);
//
//            call.enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    List<JSONObject> apiLists = new ArrayList<>();
//                    try {
//                        if (response.code() == 400) {
//                            JSONObject jsonObject1 = new JSONObject(response.errorBody().string());
//                            int statusCode = jsonObject1.getInt("statusCode");
//                            String statusMessage = jsonObject1.getString("statusMessage");
//                            System.out.println("ERROR " + statusCode + " MSG: " + statusMessage);
//                            Toast.makeText(getApplicationContext(), statusMessage, Toast.LENGTH_SHORT).show();
//                        } else {
//                            String responseData = response.body().string();
//                            JSONObject jsonObject1 = new JSONObject(responseData);
//                            int statusCode = jsonObject1.getInt("statusCode");
//                            String statusMessage = jsonObject1.getString("statusMessage");
//                            if (statusCode == 200 && statusMessage.equals("SUCCESS")) {
//                                apiLists.clear();
//                                JSONArray data = jsonObject1.getJSONArray("data");
//                                for (int i = 0; i < data.length(); i++) {
//                                    JSONObject json = data.getJSONObject(i);
//                                    apiLists.add(json);
//                                }
//
//                                System.out.println("DATA SIZE MY ADAPTER2 : " + apiLists.size());
//
//                                MyAdapter2 myAdapter = new MyAdapter2(api_register.this, apiLists);
//                                listApiRV.setAdapter(myAdapter);
//                                listApiRV.setLayoutManager(new LinearLayoutManager(api_register.this));
//
//                            }
//                            System.out.println(responseData);
//
//                        }
//                        loadingDialog.dismiss();
//
//                    } catch (Exception e) {
//                        System.out.println("FAIL111" + e.getMessage());
//                        e.printStackTrace();
//                        loadingDialog.dismiss();
//                    }
//
//
//
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                    //
//                    System.out.println("FAIL");
//                    t.printStackTrace();
//                    loadingDialog.dismiss();
//
//                }
//            });

        } catch (Exception ex) {
            Toast.makeText(api_register.this, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            loadingDialog.dismiss();
        }
    }
}