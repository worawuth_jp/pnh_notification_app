package com.example.pnhnotifier;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NotificationFragment<retrofit> extends Fragment {
    RecyclerView recyclerView;
    SharedPreferences sh;
    LoadingDialog loadingDialog;
    View view;
    Retrofit retrofit;
    int userId;
    private Handler mHandler = new Handler();

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Context context = this.getActivity();
        sh = context.getSharedPreferences("store", Context.MODE_APPEND);
        userId = sh.getInt("userId", 0);
        ApiNotificationDB apiNotificationDB= new ApiNotificationDB(context);
        apiNotificationDB.showAll();

        view = inflater.inflate(R.layout.fragment_notifications, container, false);
        recyclerView = view.findViewById(R.id.rvMessages);
        loadingDialog = new LoadingDialog(context);
        loadingDialog.start();

        //refresh(view,10000,userId,retrofit);
        startService();
        Intent sendMessageIntent = new Intent(getContext(), com.example.pnhnotifier.ConsumeMessageFromApi.class);
        getActivity().startService(sendMessageIntent);
        return view;
    }

    @Override
    public void onResume() {
        System.out.println("RESUME.....");
        refresh(view, 5000, userId, retrofit);
        super.onResume();
    }

    private void ShowNoti(View view, int userId, Retrofit retrofit) {
        ApiNotificationDB apiNotificationDB = new ApiNotificationDB(getContext());
        String username = sh.getString("cardId","");
        List<LogApi> displayApi = new ArrayList<>();
        displayApi = apiNotificationDB.getDisplayLog(username);
        List<LogApi> apiLists = new ArrayList<>();
        apiLists.clear();
        apiLists = displayApi;

        //System.out.println("DATA SIZE : " + apiLists.size());
        MyAdapter myAdapter = new MyAdapter(view.getContext(), apiLists);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        System.out.println(displayApi);
        loadingDialog.dismiss();

    }

    private void refresh(View v, int milliseconds, int userId, Retrofit retrofit) {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {

            @Override
            public void run() {
                System.out.println("Refresh()");
                if (sh.getInt("userId", 0) != 0) {
                    ShowNoti(v, userId, retrofit);
                    startService();
                }

                handler.postDelayed(this, milliseconds);
            }
        };

        handler.postDelayed(runnable, 1000);
    }

    private void startService() {
        Intent serviceIntentNoti = new Intent(getContext(), com.example.pnhnotifier.NotifactionService.class);
        getActivity().startService(serviceIntentNoti);

        //ContextCompat.startForegroundService(getContext(), serviceIntentNoti);
    }


}