package com.example.pnhnotifier;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EditApiActivity extends AppCompatActivity {

    TextView backBtn,editBtn;
    private Dialog dialog;
    SharedPreferences sp;
    EditText txtAPIkey, txtAPIname, txtAPIfreqtime,txtTitleTemp,txtContentTemp,displayNumTxt;
    int apiId = 0;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_api);
        sp = getApplication().getSharedPreferences("store", Context.MODE_APPEND);
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                apiId= 0;
            } else {
                apiId= extras.getInt("apiId");
            }
        } else {
            apiId= (int) savedInstanceState.getSerializable("apiId");
        }

        txtAPIfreqtime = findViewById(R.id.txtAPIfreqtime);
        txtAPIkey = findViewById(R.id.txtAPIkey);
        txtAPIname = findViewById(R.id.txtAPIname);
        displayNumTxt = findViewById(R.id.display_num);
        ConnectDBServices connectDBServices = new ConnectDBServices();
        try {
            Query query = new Query();
            String username = sp.getString("cardId","");
            Connection con = connectDBServices.getCon();
            String sql = query.Find_API_BY_USERNAME_ID;
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1,username);
            ps.setInt(2,apiId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                txtAPIkey.setText(rs.getString("api_key"));
                txtAPIname.setText(rs.getString("api_name"));
                txtAPIfreqtime.setText(Integer.toString(rs.getInt("api_interval")));
                txtTitleTemp.setText(rs.getString("title_temp"));
                txtContentTemp.setText(rs.getString("content_temp"));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


//        ApiServices apiServices = new ApiServices();
//        Retrofit retrofit = apiServices.getRetrofit();
//
//        ApiServices.GetApiById services = retrofit.create(ApiServices.GetApiById.class);
//        Call<ResponseBody> call = services.getApi(sp.getInt("userId",0),false,apiId);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    if (response.code() == 400) {
//                        JSONObject jsonObject1 = new JSONObject(response.errorBody().string());
//                        int statusCode = jsonObject1.getInt("statusCode");
//                        String statusMessage = jsonObject1.getString("statusMessage");
//                        System.out.println("ERROR " + statusCode + " MSG: " + statusMessage);
//                        Toast.makeText(getApplicationContext(), statusMessage, Toast.LENGTH_SHORT).show();
//                    } else {
//                        String responseData = response.body().string();
//                        JSONObject jsonObject1 = new JSONObject(responseData);
//                        int statusCode = jsonObject1.getInt("statusCode");
//                        String statusMessage = jsonObject1.getString("statusMessage");
//                        if (statusCode == 200 && statusMessage.equals("SUCCESS")) {
//                            JSONObject data = jsonObject1.getJSONObject("data");
//
//                            txtAPIkey.setText(data.getString("api_key"));
//                            txtAPIname.setText(data.getString("api_name"));
//                            txtAPIfreqtime.setText(data.getString("api_interval"));
//                            txtTitleTemp.setText(data.getString("title_temp"));
//                            txtContentTemp.setText(data.getString("content_temp"));
//
//                        }
//                        System.out.println(responseData);
//                    }
//
//                } catch (Exception e) {
//                    System.out.println("FAIL111" + e.getMessage());
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                //
//                System.out.println("FAIL");
//                t.printStackTrace();
//
//            }
//        });

        backBtn = findViewById(R.id.EditBackApi);
        editBtn = findViewById(R.id.btnEditApi);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editData();
                dialog.show();
            }
        });

    }

    public void editData(){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_dialog_edit);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            dialog.getWindow().setBackgroundDrawable(getDrawable(R.drawable.custom_dialog_background));
        }
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false); //Optional
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation; //Setting the animations to dialog

        Button Okay = dialog.findViewById(R.id.btn_okay);
        Button Cancel = dialog.findViewById(R.id.btn_cancel);

        Okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ConnectDBServices connectDBServices = new ConnectDBServices();
                    String sql = Query.UPDATE_API_BY_ID;
                    Connection con = connectDBServices.getCon();
                    PreparedStatement ps = con.prepareStatement(sql);
                    ps.setString(1,txtAPIkey.getText().toString().trim());
                    ps.setString(2,txtAPIname.getText().toString().trim());
                    ps.setInt(3,Integer.parseInt(txtAPIfreqtime.getText().toString().trim()));
                    ps.setString(4,sp.getString("cardId",""));
                    ps.setInt(5,Integer.parseInt(displayNumTxt.getText().toString().trim()));
                    ps.setInt(6,apiId);
                    int rowCount = ps.executeUpdate();
                    ps.close();
                    con.close();
                    if(rowCount>0){
                        Log.d("SAVE-LOG","EDIT API");
                        finish();
                    }
                    dialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}