package com.example.pnhnotifier;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NotificationDetailActivity extends AppCompatActivity {
    TextView api_name,api_title,responseTxt,textTime;
    Button backButtonDt;
    RecyclerView recyclerView;
    int pageNo = 1,pageSize = 30;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_detail);

        @SuppressLint("WrongConstant") SharedPreferences sp = getApplicationContext().getSharedPreferences("store", Context.MODE_APPEND);


//        api_name = findViewById(R.id.api_name);
//        api_title = findViewById(R.id.api_title);
//        responseTxt = findViewById(R.id.response);
        backButtonDt = findViewById(R.id.back_button_dt);
//        textTime = findViewById(R.id.textTime);
        recyclerView = findViewById(R.id.rvDetailMessages);

        int userId = sp.getInt("userId",0);
        int apiId = 0;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                apiId= 0;
            } else {
                apiId= extras.getInt("apiId");
            }
        } else {
            apiId= (int) savedInstanceState.getSerializable("apiId");
        }

        backButtonDt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ApiNotificationDB apiNotificationDB = new ApiNotificationDB(this);
        List<LogApi> apiLists = null;
        try {
            apiLists = apiNotificationDB.getDetailLog(pageSize,apiId);
            MyAdapter3 myAdapter = new MyAdapter3(NotificationDetailActivity.this, apiLists);
            recyclerView.setAdapter(myAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(NotificationDetailActivity.this));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


//        ApiServices apiServices = new ApiServices();
//        Retrofit retrofit = apiServices.getRetrofit();
//
//        ApiServices.GetApiLog services = retrofit.create(ApiServices.GetApiLog.class);
//        Call<ResponseBody> call = services.getLog(userId,apiId,pageNo,pageSize);
//
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                List<JSONObject> apiLists = new ArrayList<>();
//                try {
//                    if (response.code() == 400) {
//                        JSONObject jsonObject1 = new JSONObject(response.errorBody().string());
//                        int statusCode = jsonObject1.getInt("statusCode");
//                        String statusMessage = jsonObject1.getString("statusMessage");
//                        System.out.println("ERROR " + statusCode + " MSG: " + statusMessage);
//                        Toast.makeText(getApplicationContext(), statusMessage, Toast.LENGTH_SHORT).show();
//                    } else {
//                        String responseData = response.body().string();
//                        JSONObject jsonObject1 = new JSONObject(responseData);
//                        int statusCode = jsonObject1.getInt("statusCode");
//                        String statusMessage = jsonObject1.getString("statusMessage");
//                        if (statusCode == 200 && statusMessage.equals("SUCCESS")) {
//                            apiLists.clear();
//                            JSONArray data = jsonObject1.getJSONArray("data");
//                            for (int i = 0; i < data.length(); i++) {
//                                JSONObject json = data.getJSONObject(i);
//                                apiLists.add(json);
//
//                            }
//
//                            MyAdapter3 myAdapter = new MyAdapter3(NotificationDetailActivity.this, apiLists);
//                            recyclerView.setAdapter(myAdapter);
//                            recyclerView.setLayoutManager(new LinearLayoutManager(NotificationDetailActivity.this));
//
//                        }
//                        System.out.println(responseData);
//                    }
//
//                } catch (Exception e) {
//                    System.out.println("FAIL111" + e.getMessage());
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                //
//                System.out.println("FAIL");
//                t.printStackTrace();
//
//            }
//        });
    }
}