package com.example.pnhnotifier;

public class LogApi {
    public int api_id;
    public String api_name;
    public String api_title;
    public String api_message;
    public String api_timestamp;

    public LogApi(int api_id,String api_name, String api_title, String api_message,String api_timestamp){
        this.api_id = api_id;
        this.api_name = api_name;
        this.api_title = api_title;
        this.api_message = api_message;
        this.api_timestamp = api_timestamp;
    }
}
