package com.example.pnhnotifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class LogWriter {
    private String path = "logDebug.log";
    public void writeLog(String t){
        String s = new String(t.getBytes(StandardCharsets.UTF_8),StandardCharsets.UTF_8);
//        String.format("Writer > %s\n",new String(t.getBytes(StandardCharsets.UTF_8),StandardCharsets.UTF_8))
        String val = String.format("Writer > %s\n",new String(t.getBytes(StandardCharsets.UTF_8),StandardCharsets.UTF_8));
        StringBuilder _sbVal = new StringBuilder(val);
        try {
            File f = new File(path);

            Charset css = (Charset) Charset.availableCharsets().get("UTF-8");
            FileOutputStream fo = new FileOutputStream(path,true);
            Writer wor = new OutputStreamWriter(fo, css);
            if (!f.canRead()) {
                wor.write(_sbVal.toString());
            } else {
                wor.append(_sbVal.toString());
            }
            wor.flush();
            wor.close();
            fo.close();
        } catch (Exception ex) {
            System.out.println("Exception : " + ex.getMessage());
        }
    }
}
