package com.example.pnhnotifier;

public class User {
    final String userId;
    final String username;
    final String password;
    final String phone;
    final String cardId;
    final String enable;

    public User(String userId,String username,String password,String phone,String cardId,String enable){
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.phone = phone;
        this.cardId = cardId;
        this.enable = enable;
    }
}
