package com.example.pnhnotifier;

import java.util.List;

public class ResponseWithData {
    public int statusCode;
    public String statusMessage;
    public List<Object> data;
}
