package com.example.pnhnotifier;

public class Api {

    public int api_id;
    public String api_key;
    public String api_name;
    public int api_interval;
    public String username;
    public int display_num;
    public String enable;
    public String created_at;

    public Api(){

    }

    public Api(int api_id, String api_key, String api_name, int api_interval, String username, int display_num, String enable, String created_at) {
        this.api_id = api_id;
        this.api_key = api_key;
        this.api_name = api_name;
        this.api_interval = api_interval;
        this.username = username;
        this.enable = enable;
        this.created_at = created_at;
    }
}
