package com.example.pnhnotifier;

import android.os.StrictMode;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectDBServices {
    Constant constant = new Constant();
    Connection con ;


    public Connection getCon() throws ClassNotFoundException, SQLException {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Class.forName("com.mysql.jdbc.Driver");
        con= DriverManager.getConnection(
                "jdbc:mysql://"+constant.HOST_IP+":"+constant.PORT+"/"+constant.DB_NAME+"?allowPublicKeyRetrieval=true&useSSL=false",constant.USER_DB,constant.PASS_DB);
        return this.con;
    }

    public void closeConnection() throws SQLException {
        con.close();
    }
}
