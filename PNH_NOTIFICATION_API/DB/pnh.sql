-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2022 at 01:21 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pnh`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_notifications`
--

CREATE TABLE `api_notifications` (
  `api_id` int(11) NOT NULL,
  `api_key` longtext NOT NULL,
  `api_name` varchar(500) NOT NULL,
  `api_title` varchar(200) DEFAULT NULL,
  `api_interval` decimal(20,2) NOT NULL,
  `title_temp` varchar(200) NOT NULL DEFAULT '',
  `content_temp` longtext NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL,
  `enable` varchar(2) NOT NULL DEFAULT 'Y',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `api_notifications`
--

INSERT INTO `api_notifications` (`api_id`, `api_key`, `api_name`, `api_title`, `api_interval`, `title_temp`, `content_temp`, `user_id`, `enable`, `created_at`) VALUES
(19, 'https://cs.lbmoph.org/api_msg/getmsg?uname=test', 'users', 'Msg : ทดสอบส่งข้อความ', '100.00', 'Msg : ${msg}', 'เนื้อหา คือ ${msg}', 17, 'Y', '2022-02-08 07:03:43'),
(20, 'https://cs.lbmoph.org/api_msg/getmsg?uname=test', 'testName1', 'Msg : ทดสอบส่งข้อความ', '65.00', 'Msg : ${msg}', 'เนื้อหา คือ ${msg}', 1, 'Y', '2022-02-08 07:03:43'),
(26, 'https://jsonplaceholder.typicode.com/users', 'users', 'Name : Leanne Graham', '100.00', 'Name : ${name}', 'Email คือ ${email}', 1, 'Y', '2022-02-11 11:01:31'),
(27, 'https://jsonplaceholder.typicode.com/users', 'users', 'Name : Leanne Graham', '120.00', 'Name : ${name}', 'Email คือ ${email}', 1, 'Y', '2022-02-11 11:20:09');

-- --------------------------------------------------------

--
-- Table structure for table `log_noti`
--

CREATE TABLE `log_noti` (
  `api_id` int(11) NOT NULL,
  `response` longtext NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `user_id` int(11) NOT NULL,
  `num_row` int(11) NOT NULL,
  `isNotification` int(11) NOT NULL,
  `sound` int(11) NOT NULL,
  `isRepeat` int(11) NOT NULL,
  `isDefaultVolumn` int(11) NOT NULL,
  `volumn` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `isVibate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(50) NOT NULL,
  `user_phone` varchar(10) DEFAULT NULL,
  `user_card_id` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `enable` varchar(1) NOT NULL DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_username`, `user_phone`, `user_card_id`, `password`, `enable`) VALUES
(1, 'test_username', '0811111111', '1111', '1111', 'Y'),
(15, 'u', '1111', '1112', '0000', 'Y'),
(16, '1113', '1113', '1113', '1113', 'Y'),
(17, 'user1', '0881112113', '1114', '1114', 'Y');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_notifications`
--
ALTER TABLE `api_notifications`
  ADD PRIMARY KEY (`api_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `log_noti`
--
ALTER TABLE `log_noti`
  ADD KEY `api_id` (`api_id`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `response` (`response`(768));

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_card_id_2` (`user_card_id`),
  ADD KEY `user_username` (`user_username`),
  ADD KEY `user_card_id` (`user_card_id`),
  ADD KEY `password` (`password`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_notifications`
--
ALTER TABLE `api_notifications`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `api_notifications`
--
ALTER TABLE `api_notifications`
  ADD CONSTRAINT `api_notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `log_noti`
--
ALTER TABLE `log_noti`
  ADD CONSTRAINT `log_noti_ibfk_1` FOREIGN KEY (`api_id`) REFERENCES `api_notifications` (`api_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `setting`
--
ALTER TABLE `setting`
  ADD CONSTRAINT `setting_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
