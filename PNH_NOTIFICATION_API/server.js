const http = require("http");
const app = require("./app");
const ip = require("ip");
const { getApiNotification } = require("./Middleware/ApiCronjobMiddleWare");

const port = 8080;
app.set("port", port);

const server = http.createServer(app);
server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }
  console.error(error);
  throw error;
}

function onListening() {
  global.apiStorage = [];
  global.apiRunningInterval = [];
  global.apiIntervalId = [];
  const addr = server.address();
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  getApiNotification();
  console.log("Listening on " + bind);
  console.log("PUBLIC IP: ", ip.address());
}
