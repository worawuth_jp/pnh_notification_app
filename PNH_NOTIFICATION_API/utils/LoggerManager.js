const log4js = require("log4js")

class LoggerManager {
    getLogger(className){
        const logger = log4js.getLogger(className)
        logger.level = "debug";
        return logger;
    }
}

module.exports = new LoggerManager()