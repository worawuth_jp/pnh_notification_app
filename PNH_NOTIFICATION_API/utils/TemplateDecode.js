const _ = require("lodash");
const LoggerManager = require("./LoggerManager");
const logger = LoggerManager.getLogger("TemplateDecode Utils")

class TemplateDecode {
    async _decodeMessage(msg,data){
        if(typeof data === "string"){
            data = JSON.parse(data);
        }
    
        if(Array.isArray(data)){
            logger.info("ARRAY : ",data);
            if(data.length > 0){
                msg = _.template(msg);
                msg = msg(data[0]);
            }
        }else if(typeof data === 'object'){
            logger.info("OBJECT : ",data);
            msg = _.template(msg);
            msg = msg(data);
        }
    
        return msg
    }
}

module.exports = new TemplateDecode();