const { HTTP_SUCCESS_CODE } = require("../constants/http");
const { SUCCESS_MESSAGE } = require("../constants/message");
const ResponseModel = require("../models/responseModel");

class TransformModelUtils {
    async transformResponseModel(){
        const responseModel = new ResponseModel()
        responseModel.statusCode = HTTP_SUCCESS_CODE
        responseModel.statusMessage = SUCCESS_MESSAGE
        return responseModel;
    }

    async transformResponseWithDataModel(data){
        const responseModel = new ResponseModel()
        responseModel.statusCode = HTTP_SUCCESS_CODE
        responseModel.statusMessage = SUCCESS_MESSAGE
        responseModel.data = data
        return responseModel;
    }

    async transformResponseWithPaginateModel(data,total){
        const responseModel = new ResponseModel()
        responseModel.statusCode = HTTP_SUCCESS_CODE;
        responseModel.statusMessage = SUCCESS_MESSAGE;
        responseModel.totalData = total;
        responseModel.data = data;
        return responseModel;
    }
}

module.exports = new TransformModelUtils();