class ResponseModelWithPagging{
    get getStatusCode(){
        return this.statusCode;
    }
    set setStatusCode(code){
        this.statusCode = code;
    }

    get getStatusMessage(){
        return this.statusMessage;
    }
    set setStatusMessage(message){
        this.statusMessage = message;
    }

    get getTotalData(){
        return this.totalData;
    }
    set setTotalData(total){
        this.totalData = total;
    }

    get getData(){
        return this.data;
    }
    set setData(data){
        this.data = data;
    }

    get getTotal(){
        return this.total;
    }
    set setTotal(total){
        this.total = total;
    }
}

module.exports = ResponseModelWithPagging;