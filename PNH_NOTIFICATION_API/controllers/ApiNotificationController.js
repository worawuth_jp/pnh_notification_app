const http = require("../constants/http");
const message = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const ApiNotificationService = require("../services/ApiNotificationService");
const LoggerManager = require("../utils/LoggerManager");
const TransformModelUtils = require("../utils/TransformModelUtils");
const logger = LoggerManager.getLogger("ApiNotificationController");
//Controller จะทำการ validate request และ ส่งไปทำงานด้าน ลอจิกเกี่ยวกับการคำนวณหรือ คิวรี่ในเซอร์วิช เมื่อได้ ข้อมูลมา จะทำการจัดรูปและส่งออกไปเป็น response

class ApiNotificationController {
    async listApi(req) {
        try {
            const all = req.all === 'true'? true : false
            const apiId = req.api_id;
            const apiName = req.api_name;
            const search = req.search;
            const userId = req.user_id
            logger.info("All Value",all)
            let resultSuccess;
            if(apiId || apiName || search || userId){
                resultSuccess = await ApiNotificationService.listApiNotify(all,apiId,apiName,search,userId)
            }else{
                resultSuccess = await ApiNotificationService.listAllApiNotify(all,userId);
            }
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("ApiNotificationController.listApi() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async addApi(body) {
        try {
            const isUndefined = body.api_key && body.api_name && body.api_interval && body.user_id && body.api_title && body.title_temp && body.content_temp && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const apiKey = body.api_key;
            const apiName = body.api_name;
            const apiInterval = body.api_interval;
            const apiTitle = body.api_title;
            const userId = body.user_id;
            const titleTemp = body.title_temp;
            const contentTemp = body.content_temp;
            const resultSuccess = await ApiNotificationService.addApiNptify(apiKey,apiName,apiTitle,apiInterval,userId,titleTemp,contentTemp);
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("ApiNotificationController.addApi() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async updateApi(body) {
        try {
            const isUndefined = body.api_key && body.api_name && body.api_interval && body.api_id && body.user_id && body.enable && body.title_temp && body.content_temp && 1;
            if(!isUndefined || !(body.enable === 'Y' || body.enable === 'N')){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const apiKey = body.api_key;
            const apiName = body.api_name;
            const apiInterval = body.api_interval;
            const apiId = body.api_id;
            const userId = body.user_id;
            const enable = body.enable;
            const titleTemp = body.title_temp;
            const contentTemp = body.content_temp;
            const resultSuccess = await ApiNotificationService.updateApiNptify(apiId,apiKey,apiName,apiInterval,userId,enable,titleTemp,contentTemp);
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("ApiNotificationController.updateApi() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async updateStatusApi(body) {
        try {
            const isUndefined = body.api_id && body.enable && 1;
            if(!isUndefined || !(body.enable === "Y" || body.enable === "N")){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const apiId = body.api_id;
            const enable = body.enable;
            const resultSuccess = await ApiNotificationService.updateStatusApiNptify(apiId,enable);
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("ApiNotificationController.updateStatusApi() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async deleteApi(body) {
        try {
            const isUndefined = body.api_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }

            const apiId = body.api_id;
            const resultSuccess = await ApiNotificationService.deleteApiNotification(apiId);
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("ApiNotificationController.deleteApi() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async listDetailApi(req) {
        try {
            const apiKey = req.api_key;
            let resultSuccess;
            if(!apiKey || void 0 === apiKey){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            resultSuccess = await ApiNotificationService.listDetailApi(apiKey);
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("ApiNotificationController.listDetailApi() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }
}
module.exports = new ApiNotificationController();