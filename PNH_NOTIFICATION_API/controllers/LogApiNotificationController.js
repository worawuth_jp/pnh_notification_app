const http = require("../constants/http");
const message = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const LogApiNotiServices = require("../services/LogApiNotiServices");
const LoggerManager = require("../utils/LoggerManager");
const TransformModelUtils = require("../utils/TransformModelUtils");
const logger = LoggerManager.getLogger("LogApiNotoficationController")

class LogApiNotoficationController {
    async listLog(req) {
        try {
            const userId = req.user_id;
            const apiId = req.api_id;
            const pageNo = Number(req.page_no);
            const pageSize = Number(req.page_size);

            let resultSuccess;
            
            resultSuccess = await LogApiNotiServices.listAllLogApiNotify(userId,apiId,pageNo,pageSize)

            const responseSuccess = await TransformModelUtils.transformResponseWithPaginateModel(resultSuccess.data,resultSuccess.totalData)
            return responseSuccess;
        } catch (error) {
            logger.error("LogApiNotoficationController.listLog() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async display(req) {
        try {
            const userId = req.user_id;
            const apiId = req.api_id;

            let resultSuccess;
            
            resultSuccess = await LogApiNotiServices.display(userId,apiId)

            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("LogApiNotoficationController.display() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async deleteNotiApi(req) {
        try {
            const userId = req.user_id;
            const apiId = req.api_id;

            let resultSuccess;
            if(apiId == void 0 || apiId === "" || apiId == 0){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            
            resultSuccess = await LogApiNotiServices.deleteApi(userId,apiId)

            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("LogApiNotoficationController.display() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    
}
module.exports = new LogApiNotoficationController();