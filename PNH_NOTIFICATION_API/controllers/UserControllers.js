const http = require("../constants/http");
const message = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const UserServices = require("../services/UserServices");
const LoggerManager = require("../utils/LoggerManager");
const TransformModelUtils = require("../utils/TransformModelUtils");
const logger = LoggerManager.getLogger("UserControllers")

class UserControllers {
    async listUsers(req) {
        try {
            const all = req.all === 'true'? true : false
            const userId = req.user_id;
            const username = req.user_username;
            const search = req.search;
            const cardId = req.user_card_id
            logger.info("All Value",all)
            let resultSuccess;
            if(username || userId || search || cardId){
                resultSuccess = await UserServices.listUser(all,userId,username,search,cardId)
            }else{
                resultSuccess = await UserServices.listAllUsers(all);
            }
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("UserControllers.listUsers() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async addUser(body) {
        try {
            const isUndefined = body.user_username && body.user_phone && body.user_card_id && body.user_password && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const username = body.user_username;
            const phone = body.user_phone;
            const cardId = body.user_card_id;
            const password = body.user_password;
            const resultSuccess = await UserServices.addUser(username,password,phone,cardId);
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("UserControllers.addUser() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async updateUser(body) {
        try {
            const isUndefined = body.user_username && body.user_phone && body.user_card_id && body.user_password && body.enable && body.user_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const username = body.user_username;
            const cardId = body.user_card_id;
            const phone = body.user_phone;
            const password = body.user_password;
            const userId = body.user_id;
            const enable = body.enable;
            const resultSuccess = await UserServices.updateUser(userId,username,phone,cardId,password,enable);
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("UserControllers.updateUser() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async updateStatusUser(body) {
        try {
            const isUndefined = body.user_id && body.enable && 1;
            if(!isUndefined || !(body.enable === "Y" || body.enable === "N")){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            const userId = body.user_id;
            const enable = body.enable;
            const resultSuccess = await UserServices.updateStatusUser(userId,enable);
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("UserControllers.updateStatusUser() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async deleteUser(body) {
        try {
            const isUndefined = body.user_id && 1;
            if(!isUndefined){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }

            const userId = body.user_id;
            const resultSuccess = await UserServices.deleteUser(userId);
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("UserControllers.deleteUser() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }

    async loginUser(body) {
        try {
            const id = body.id;
            const password = body.password;
            const isUndefined = body.id && body.password && 1;
            if(!isUndefined || id === '' || password === ''){
                throw new ClientError(http.CLIENT_ERROR_BAD_REQUEST_CODE,http.CLIENT_ERROR_BAD_REQUEST_CODE,message.CLIENT_ERROR_BAD_REQUEST_MSG);
            }
            let resultSuccess = await UserServices.loginUser(id,password);
            const responseSuccess = await TransformModelUtils.transformResponseWithDataModel(resultSuccess)
            return responseSuccess;
        } catch (error) {
            logger.error("UserControllers.loginUser() Exception : ",error)
            if(error instanceof ClientError){
                throw new ClientError(error.http,error.code,error.data.message, error.stack);
            }else{
                throw new SystemError(error.http, error.code,error.data.message, error.stack);
            }
        }
    }
}
module.exports = new UserControllers();