const { isNull } = require("lodash");
const request = require("request-promise");
const DBAPI = require("../API/DBApi");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const { updateApiNotification, clearApiInterval } = require("../Middleware/ApiCronjobMiddleWare");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const LoggerManager = require("../utils/LoggerManager");
const TemplateDecode = require("../utils/TemplateDecode");
let logger =  LoggerManager.getLogger("ApiNotificationService")

class ApiNotificationService {
    async listAllApiNotify(all,userId){
        // เรียกการ Connection DB
        const dbApi = new DBAPI();
        try {
            let sql = "SELECT * FROM api_notifications WHERE 1=1";
            let queryParams = [];
            //Where Clase Condition
            if(!all){
                sql += " AND enable='Y'"
            }

            if(userId){
                sql += " AND user_id=?"
                queryParams.push(userId)
            }

            //query เพื่อให้ได้ผลลัพธ์
            const resultSet = await dbApi.query(sql,queryParams);
            logger.info("RESULT SET DATA ----> ",resultSet);
            for await (const data of resultSet){
                //จัดรูปข้อมูล enable คือฟิลด์ที่บอกว่าข้อมูลนี้ถูกเปิดใช้งานไหม
                data.isEnable = data.enable == 'Y' ? true : false;
            }
            return resultSet;
        } catch (error) {
            logger.info("ApiNotificationService.listAllApiNotify() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async listApiNotify(all,apiId,apiName,search,userId){
        const dbApi = new DBAPI();
        try {
            let sql = "SELECT * FROM api_notifications WHERE 1=1";
            let queryParams = [];
            if(!all){
                sql += " AND enable='Y'"
            }

            if(apiName){
                sql += " AND api_name=?";
                queryParams.push(apiName)
            }

            if(search){
                sql += " AND (api_id LIKE ? OR api_name LIKE ? OR api_interval LIKE ?)";
                queryParams.push(`%${search}%`,`%${search}%`,`%${search}%`)
            }

            if(userId){
                sql += " AND user_id = ?";
                queryParams.push(userId)
            }

            if(apiId){
                sql += " AND api_id = ? LIMIT 1";
                queryParams.push(apiId)
            }

            let resultSet;
            resultSet = await dbApi.query(sql,queryParams);
            logger.info("RESULT SET DATA ----> ",resultSet);
            for await (const data of resultSet){
                data.isEnable = data.enable == 'Y' ? true : false;
            }
            if(apiId){
                if(resultSet.length > 0){
                    resultSet = resultSet[0];
                }else{
                    resultSet = {}
                }
            }
            return resultSet;
        } catch (error) {
            logger.info("ApiNotificationService.listAllApiNotify() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async addApiNptify(apiKey,apiName,apiTitle,apiInterval,userId,titleTemp,contentTemp){
        const dbApi = new DBAPI();
        try {
                let responseString = await request(apiKey);
                let title = await TemplateDecode._decodeMessage(titleTemp,responseString);
                logger.info("RESPONSE : ",responseString);
                let sql = "INSERT INTO api_notifications (api_key,api_name,api_title,api_interval,title_temp,content_temp,user_id) VALUES(?,?,?,?,?,?,?)";
                let queryParams = [apiKey,apiName,title,apiInterval,titleTemp,contentTemp,userId];
                const resultSet = await dbApi.query(sql,queryParams);
                let data = {
                    msg: MSG.SUCCESS_INSERT_MESSAGE,
                    isInsert: true,
                    id: resultSet.insertId
                };

                sql = "SELECT * FROM api_notifications WHERE api_id = ?";
                const resultData = await dbApi.query(sql,[data.id])
                if(resultData.length>0){
                    apiStorage.push(resultData[0])
                    updateApiNotification()
                }
                
                if(!resultSet){
                    data = {
                        msg: MSG.FAILURE_INSERT_MESSAGE,
                        isInsert: false,
                    };
                }

                return data;``
        } catch (error) {
            logger.info("ApiNotificationService.addApiNptify() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                if(error.sqlState){
                    throw new SystemError(HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,MSG.CLIENT_ERROR_BAD_REQUEST_MSG_2)
                }
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async updateApiNptify(apiId,apiKey,apiName,apiInterval,userId,enable,titleTemp,contentTemp){
        const dbApi = new DBAPI();
        try {
            let responseString = await request(apiKey);
            let title = await TemplateDecode._decodeMessage(titleTemp,responseString);
            logger.info("RESPONSE : ",responseString);
            let sql = "UPDATE api_notifications SET api_key=?,api_name=?,api_title=?,api_interval=?,enable=?,title_temp=?,content_temp=? WHERE api_id=? AND user_id=?";
            let queryParams = [apiKey,apiName,title,apiInterval,enable,titleTemp,contentTemp,apiId,userId];
            const resultSet = await dbApi.query(sql,queryParams);
            let data = {
                msg: MSG.SUCCESS_UPDATE_MESSAGE,
                isUpdate: true,
                id: apiId
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_UPDATE_MESSAGE,
                    isUpdate: false,
                };
            }
            sql = "SELECT * FROM api_notifications WHERE enable='Y' AND api_id ORDER BY api_interval ASC"
            queryParams = [apiId]
            const result = dbApi.query(sql,queryParams);
            if(result.length > 0){
                const index = _.findIndex(apiStorage,_.find(apiStorage,(item)=>item.api_id===apiId));
                if(result[0].api_interval !== apiStorage[index].api_interval || result[0].api_key !== apiStorage[index].api_key){

                }else{
                    apiStorage[index] = result[0];
                    apiRunningInterval[index] = result[0]
                   
                }
                
            }
            updateApiNotification()

            return data;
        } catch (error) {
            logger.info("ApiNotificationService.updateApiNptify() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                if(error.sqlState){
                    throw new SystemError(HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,MSG.CLIENT_ERROR_BAD_REQUEST_MSG_2)
                }
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async updateStatusApiNptify(apiId,enable){
        const dbApi = new DBAPI();
        try {
            let sql = "UPDATE api_notifications SET enable=? WHERE api_id=?";
            let queryParams = [enable,apiId];
            const resultSet = await dbApi.query(sql,queryParams);
            let data = {
                msg: MSG.SUCCESS_UPDATE_MESSAGE,
                isUpdate: true,
                id: apiId
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_UPDATE_MESSAGE,
                    isUpdate: false,
                };
            }
            return data;
        } catch (error) {
            logger.info("ApiNotificationService.updateStatusApiNptify() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async deleteApiNotification(apiId){
        const dbApi = new DBAPI();
        try {
            let sql = "DELETE FROM api_notifications WHERE api_id=?";
            let queryParams = [apiId];
            const resultSet = await dbApi.query(sql,queryParams);
            let data = {
                msg: MSG.SUCCESS_DELETE_MESSAGE,
                isDelete: true,
                id: apiId
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_DELETE_MESSAGE,
                    isDelete: false,
                };
            }
            _updateStorage();
            return data;
        } catch (error) {
            logger.info("ApiNotificationService.deleteApiNotification() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }


    async listDetailApi(apiKey){
        // เรียกการ Connection DB
        const dbApi = new DBAPI();
        try {
            let responseString = await request(apiKey);
            let resultSet = {};
            logger.info("RESPONSE : ",responseString);
            logger.info("TYPE OF : ",typeof responseString);
            if(typeof responseString === "string"){
                responseString = JSON.parse(responseString);
            }
            logger.info("TYPE OF 2 : ",typeof responseString);
            if(Array.isArray(responseString)){
                logger.info("ARRAY : ",responseString);
                if(responseString.length > 0){
                    resultSet.keys = Object.keys(responseString[0]).map(val=>{
                        return {key_name: val,type: typeof responseString[0][val]}
                    })
                }
            }else if(typeof responseString === 'object'){
                logger.info("OBJECT : ",responseString);
                resultSet.keys = resultSet.key_name = Object.keys(responseString).map(val=>{
                    return {key_name: val,type: typeof responseString[val]}
                })
            }

            return resultSet;
        } catch (error) {
            logger.info("ApiNotificationService.listDetailApi() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }
}
module.exports = new ApiNotificationService()

const _updateStorage = (index = null)=>{
    if(isNull(index)){
        clearApiInterval()
    }else{
        clearInterval(index);
    }

    updateApiNotification()
    
};
