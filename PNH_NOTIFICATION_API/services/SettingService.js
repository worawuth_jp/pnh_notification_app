const DBAPI = require("../API/DBApi");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const LoggerManager = require("../utils/LoggerManager");
let logger =  LoggerManager.getLogger("UserServices")

class SettingService {
    async listSetting(userId){
        const dbApi = new DBAPI();
        try {
            let sql = "SELECT * FROM setting WHERE user_id=?";
            let params = [userId];
            const resultSet = await dbApi.query(sql,params);
            logger.info("RESULT SET DATA ----> ",resultSet);
            for await (const data of resultSet){
                data.isEnable = data.enable == 'Y' ? true : false;
            }
            return resultSet;
        } catch (error) {
            logger.info("SettingService.listSetting() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async manageSetting(userId,numRow,isNotification,sound,isRepeat,isDefaultVolumn,volumn,isVibate){
        const dbApi = new DBAPI();
        try {

            let sql = "SELECT IF(COUNT(*)>0,true,false) AS IS_EXIST FROM setting WHERE user_id=?"
            let queryParams = [userId];
            let resultSet = await dbApi.query(sql,queryParams);
            let isExist = resultSet[0].IS_EXIST;
            let data;
            if(!isExist){
                sql = "INSERT INTO `setting`(`user_id`, `num_row`, `isNotification`, `sound`, `isRepeat`, `isDefaultVolumn`, `volumn`, `color`, `isVibate`) VALUES (?,?,?,?,?,?,?,?,?)";
                queryParams = [userId,numRow,isNotification,sound,isRepeat,isDefaultVolumn,volumn,isVibate];
                const resultSet = await dbApi.query(sql,queryParams);
                data = {
                    msg: "ทำรายการไม่สำเร็จสำเร็จ",
                    isSuccess: false,
                    user_id: userId
                };
                if(resultSet){
                    data = {
                        msg: "ทำรายการสำเร็จ",
                        isSuccess: true,
                        user_id: userId
                    };
                }
                
            }else{
                sql = "UPDATE `setting` SET `num_row`=?, `isNotification`=?, `sound`=?, `isRepeat`=?, `isDefaultVolumn`=?, `volumn`=?, `color`=?, `isVibate`=? WHERE user_id=?";
                queryParams = [numRow,isNotification,sound,isRepeat,isDefaultVolumn,volumn,isVibate,userId];
                const resultSet = await dbApi.query(sql,queryParams);
                data = {
                    msg: "ทำรายการไม่สำเร็จสำเร็จ",
                    isSuccess: false,
                    user_id: userId
                };
                if(resultSet){
                    data = {
                        msg: "ทำรายการสำเร็จ",
                        isSuccess: true,
                        user_id: userId
                    };
                }
            }

            
            return data;
        } catch (error) {
            logger.info("SettingService.manageSetting() Exception : ",error)
            if(error.errno === 1062){
                throw new ClientError(HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,MSG.CARD_ID_DUP_ERROR_MSG)
            }
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    
}
module.exports = new SettingService()