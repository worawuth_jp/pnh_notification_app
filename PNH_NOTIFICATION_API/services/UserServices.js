const DBAPI = require("../API/DBApi");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const LoggerManager = require("../utils/LoggerManager");
let logger =  LoggerManager.getLogger("UserServices")

class UserServices {
    async listAllUsers(all){
        const dbApi = new DBAPI();
        try {
            let sql = "SELECT * FROM users";
            if(!all){
                sql += " WHERE enable='Y'"
            }
            const resultSet = await dbApi.query(sql);
            logger.info("RESULT SET DATA ----> ",resultSet);
            for await (const data of resultSet){
                data.isEnable = data.enable == 'Y' ? true : false;
            }
            return resultSet;
        } catch (error) {
            logger.info("UserServices.listAllUsers() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async listUser(all,userId,username,search,cardId){
        const dbApi = new DBAPI();
        try {
            let sql = "SELECT * FROM users WHERE 1=1";
            let queryParams = [];
            if(!all){
                sql += " AND enable='Y'"
            }

            if(userId){
                sql += " AND user_id = ? LIMIT 1";
                queryParams.push(userId)
            }

            if(username){
                sql += " AND user_username=?";
                queryParams.push(username)
            }

            if(cardId){
                sql += " AND user_card_id = ?";
                queryParams.push(cardId)
            }

            if(search){
                sql += " AND (user_id LIKE ? OR user_username LIKE ? OR user_card_id LIKE ? )";
                queryParams.push(`%${search}%`,`%${search}%`,`%${search}%`)
            }
            let resultSet;
            resultSet = await dbApi.query(sql,queryParams);
            logger.info("RESULT SET DATA ----> ",resultSet);
            for await (const data of resultSet){
                data.isEnable = data.enable == 'Y' ? true : false;
            }
            if(userId){
                if(resultSet.length > 0){
                    resultSet = resultSet[0];
                }else{
                    resultSet = {}
                }
            }
            return resultSet;
        } catch (error) {
            logger.info("UserServices.listUser() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async addUser(username,password,phone,cardId){
        const dbApi = new DBAPI();
        try {
            let sql = "INSERT INTO users (`user_username`,`user_phone`,`user_card_id`, `password`) VALUES(?,?,?,?)";
            let queryParams = [username,phone,cardId,password];
            const resultSet = await dbApi.query(sql,queryParams);
            let data = {
                msg: MSG.SUCCESS_INSERT_MESSAGE,
                isInsert: true,
                id: resultSet.insertId
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_INSERT_MESSAGE,
                    isInsert: false,
                };
            }
            return data;
        } catch (error) {
            logger.info("UserServices.addUser() Exception : ",error)
            if(error.errno === 1062){
                throw new ClientError(HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,HTTP.CLIENT_ERROR_BAD_REQUEST_CODE,MSG.CARD_ID_DUP_ERROR_MSG)
            }
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async updateUser(userId,username,phone,cardId,password,enable){
        const dbApi = new DBAPI();
        try {
            let sql = "UPDATE users SET `user_username`=?,  `user_phone`=?, `user_card_id`=?, `password`=?,`enable`=? WHERE user_id=?";
            let queryParams = [username,phone,cardId,password,enable,userId];
            const resultSet = await dbApi.query(sql,queryParams);
            let data = {
                msg: MSG.SUCCESS_UPDATE_MESSAGE,
                isUpdate: true,
                id: userId
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_UPDATE_MESSAGE,
                    isUpdate: false,
                };
            }
            return data;
        } catch (error) {
            logger.info("UserServices.updateUser() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async updateStatusUser(userId,enable){
        const dbApi = new DBAPI();
        try {
            let sql = "UPDATE users SET enable=? WHERE user_id=?";
            let queryParams = [enable,userId];
            const resultSet = await dbApi.query(sql,queryParams);
            let data = {
                msg: MSG.SUCCESS_UPDATE_MESSAGE,
                isUpdate: true,
                id: userId
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_UPDATE_MESSAGE,
                    isUpdate: false,
                };
            }
            return data;
        } catch (error) {
            logger.info("UserService.updateStatusUser() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async deleteUser(userId){
        const dbApi = new DBAPI();
        try {
            let sql = "DELETE FROM users WHERE user_id=?";
            let queryParams = [userId];
            const resultSet = await dbApi.query(sql,queryParams);
            let data = {
                msg: MSG.SUCCESS_DELETE_MESSAGE,
                isDelete: true,
                id: userId
            };
            if(!resultSet){
                data = {
                    msg: MSG.FAILURE_DELETE_MESSAGE,
                    isDelete: false,
                };
            }
            return data;
        } catch (error) {
            logger.info("UserService.deleteUser() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }

    async loginUser(userId,password){
        const dbApi = new DBAPI();
        try {
            let sql = "SELECT IF(COUNT(*)>0,true,false) IS_HAVE_USERNAME FROM users WHERE user_card_id = ?";
            let queryParams = [userId];
            const resultSet = await dbApi.query(sql,queryParams);
            let data = {};
            if(resultSet.length > 0){
                
                if(resultSet[0].IS_HAVE_USERNAME){
                    sql = "SELECT * FROM users WHERE user_card_id=? AND password=?"
                    queryParams = [userId,password]
                    let result = await dbApi.query(sql,queryParams);
                    if(result.length > 0){
                        data = {
                            msg: MSG.LOGIN_SUCCESS_MSG,
                            status: "SUCCESS",
                            user: result[0]
                        }
                    }else{
                        data = {
                            msg: MSG.INVALID_ID_PASSWORD_MSG,
                            status: "FAILURE",

                        }
                    }

                }else{
                    data = {
                        msg: MSG.CARD_ID_IS_NOT_EXIST_MSG,
                        status: "FAILURE",
                    }
                }
            }
            else{
                data = {
                    msg: MSG.LOGIN_FAILURE_MSG,
                    status: "FAILURE",
                }
            }
            return data;
        } catch (error) {
            logger.info("UserService.loginUser() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }
}
module.exports = new UserServices()