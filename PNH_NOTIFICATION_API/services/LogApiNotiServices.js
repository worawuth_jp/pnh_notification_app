const { json } = require("express");
const { isNull } = require("lodash");
const DBAPI = require("../API/DBApi");
const HTTP = require("../constants/http");
const MSG = require("../constants/message");
const { updateApiNotification, clearApiInterval } = require("../Middleware/ApiCronjobMiddleWare");
const ClientError = require("../models/ClientError");
const SystemError = require("../models/SystemError");
const LoggerManager = require("../utils/LoggerManager");
const _ = require("lodash");
const dayjs = require("dayjs");
let logger =  LoggerManager.getLogger("LogApiNotiServices")

class LogApiNotiServices {
    async listAllLogApiNotify(userId,apiId,pageNo,pageSize){
        // เรียกการ Connection DB
        const dbApi = new DBAPI();
        try {
            let sql = "SELECT ln.*,an.api_key,an.api_name,an.api_interval,an.api_title FROM log_noti ln INNER JOIN api_notifications an ON an.api_id = ln.api_id INNER JOIN users u ON u.user_id = an.user_id  WHERE 1=1";
            let countSql = "SELECT COUNT(*) AS TOTAL FROM log_noti ln INNER JOIN api_notifications an ON an.api_id = ln.api_id INNER JOIN users u ON u.user_id = an.user_id  WHERE 1=1";
            let queryParams = [];
            //Where Clase Condition
            if(userId){
                sql += " AND u.user_id=?"
                countSql += " AND u.user_id=?"
                queryParams.push(userId)
            }

            if(apiId){
                sql += " AND an.api_id=?"
                countSql += " AND an.api_id=?"
                queryParams.push(apiId)
            }

            let resultCount = await dbApi.query(countSql,queryParams);
            let total = 0
            if(resultCount){
                total = resultCount[0].TOTAL;
            }

            sql += " ORDER BY ln.created_at DESC"

            if(pageSize > 0 && pageNo > 0){
                sql += " LIMIT ? OFFSET ?";
                queryParams.push(pageSize,pageNo-1)
            }

            //query เพื่อให้ได้ผลลัพธ์
            const resultSet = await dbApi.query(sql,queryParams);
            logger.info("RESULT SET DATA ----> ",resultSet);
            for await(const data of resultSet){
                data.api_title = data.api_title?data.api_title:"";
                data.timestamp = dayjs(data.created_at).format('DD/MM/YYYY HH:mm:ss')
            }
            return {data: resultSet,totalData: total};
        } catch (error) {
            logger.info("LogApiNotiServices.listAllLogApiNotify() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }


    async display(userId,apiId){
        // เรียกการ Connection DB
        const dbApi = new DBAPI();
        try {
            let sql = "SELECT an.api_id AS ID,an.api_name,an.api_interval,an.api_title,ln.title,ln.content,\
            (SELECT created_at FROM log_noti WHERE api_id=ID ORDER BY `log_noti`.`created_at`  DESC LIMIT 1) AS CREATED_AT,\
            (SELECT response FROM log_noti WHERE api_id=ID ORDER BY `log_noti`.`created_at`  DESC LIMIT 1) AS response \
            FROM log_noti ln INNER JOIN api_notifications an ON an.api_id = ln.api_id \
            INNER JOIN users u ON u.user_id = an.user_id  WHERE 1=1";
    
            let queryParams = [];
            //Where Clase Condition
            if(userId){
                sql += " AND u.user_id=?"
                queryParams.push(userId)
            }

            if(apiId){
                sql += " AND an.api_id=?"
                queryParams.push(apiId)
            }

            sql += " GROUP BY ID ORDER BY CREATED_AT DESC"

            //query เพื่อให้ได้ผลลัพธ์
            const resultSet = await dbApi.query(sql,queryParams);
            logger.info("RESULT SET DATA ----> ",resultSet);
            for await (const data of resultSet){
                //จัดรูปข้อมูล enable คือฟิลด์ที่บอกว่าข้อมูลนี้ถูกเปิดใช้งานไหม
                data.isEnable = data.enable == 'Y' ? true : false;
                data.api_title = data.api_title? data.api_title : "" 
                data.created_at = data.CREATED_AT
                data.timestamp = dayjs(data.created_at).format('DD/MM/YYYY HH:mm:ss')

            }

            if(apiId){
                if(resultSet.length > 0){
                    return resultSet[0];
                }
            }

            return resultSet;
        } catch (error) {
            logger.info("LogApiNotiServices.display() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }


    async deleteApi(userId,apiId){
        // เรียกการ Connection DB
        const dbApi = new DBAPI();
        try {

            let sql = "SELECT IF(COUNT(*)>0,true,false) AS IS_EXIST FROM api_notifications WHERE api_id=? AND user_id=?";
            let queryParams = [apiId,userId];

            let resultSet = await dbApi.query(sql,queryParams);
            if(!resultSet[0].IS_EXIST){
                return {
                    msg: "ไม่ได้รับอนุญาตให้ลบ api นี้",
                    isDelete: false
                }
            }

            sql = "DELETE FROM log_noti WHERE api_id=?";

            queryParams = [apiId];
            //query เพื่อให้ได้ผลลัพธ์
            resultSet = await dbApi.query(sql,queryParams);
            let data = {
                msg: MSG.FAILURE_DELETE_MESSAGE,
                isDelete: false,
            }
            if(resultSet){
                data.msg = MSG.SUCCESS_DELETE_MESSAGE,
                data.isDelete = true
            }
            
            return data;
        } catch (error) {
            logger.info("LogApiNotiServices.deleteApi() Exception : ",error)
            if(error.code && error.data && error.data.message){
                if(error instanceof ClientError){
                    throw new ClientError(error.http,error.code,error.data.message);
                }else{
                    throw new SystemError(error.http, error.code,error.data.message);
                }
            }else{
                throw new SystemError(HTTP.SYSTEM_ERR_HTTP_CODE,HTTP.SYSTEM_ERR_HTTP_CODE,MSG.SYSTEM_ERROR_HTTP_MSG)
            }
        }finally{
            dbApi.closeConnection();
        }
    }


}
module.exports = new LogApiNotiServices()
