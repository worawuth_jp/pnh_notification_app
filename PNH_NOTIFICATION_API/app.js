const createError = require("http-errors");
const httpContext = require("express-http-context");
const express = require("express");
const path = require("path");
const app = express();
const cors = require("cors");

const indexRouter = require("./routes/index");
const ApiNotificationRouter = require("./routes/ApiNotificationRouter")
const UserRouter = require("./routes/UserRouter");
const LogApiRouter = require("./routes/LogApi");
const { updateApiNotification } = require("./Middleware/ApiCronjobMiddleWare");
const corsOptions = {
  origin: true,
};

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

//set cors ไม่ให้ยิงแล้ว request address ถูก block
app.use(cors(corsOptions));

app.use(express.json());
// ให้สามารถส่ง request data มาแบบ encode เข้า link ได้
app.use(express.urlencoded({ extended: false }));

//เผื่อมีการเก็บ static file
app.use(express.static(path.join(__dirname, "public")));
app.use(httpContext.middleware);

//เรียกใช้ middleware ในการยิง api notification แบบ interval
app.use(async function(req,res,next){
  await updateApiNotification()
  next()
})

//Route Mapping เป็นจุดแรกที่เมื่อยิง request เข้ามาจะวิ่งเข้าตรงนี้แล้ว จะหาเส้น api ที่ต้องเพื่อเข้าฟังก์ชั่นทำงาน
app.use("/", indexRouter);
app.use("/api",ApiNotificationRouter)
app.use("/api",UserRouter)
app.use("/api",LogApiRouter)

// 404 page Not found
app.use((err, req, res, next) => {
  next(createError(404));
});

// 500 ERROR
app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = err;

  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
