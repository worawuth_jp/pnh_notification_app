const cron = require("node-cron");
const DBAPI = require("../API/DBApi");
const LoggerManager = require("../utils/LoggerManager");
const _ = require("lodash");
var request = require("request");
const { isNull } = require("lodash");
const TemplateDecode = require("../utils/TemplateDecode");

let logger = LoggerManager.getLogger("ApiMiddleWare");
module.exports = {
  getApiNotification: async () => {
    logger.info("PREPARE API NOTIFICATION ....")
    const dbApi = new DBAPI();
    try {
      if (apiStorage.length === 0) {
        let sql = "SELECT * FROM api_notifications WHERE enable='Y'";
        const result = await dbApi.query(sql);
        logger.info("API NOTIFICATION RESULT ---> ",result)
        for await (const data of result) {
          if (!apiStorage.includes(data)) {
            apiStorage.push(data);
            apiRunningInterval.push(data);
          }
        }
        logger.info("START API NOTIFICATION ....")
      }

      apiStorage = _.orderBy(apiStorage, ["api_interval"], ["asc"]);

      for await (const [i, api] of apiStorage.entries()) {
      
        //ยิง รับข้อมูลจาก api ตาม interval
        apiIntervalId.push(setInterval(async () => {
          request(api.api_key, async function (error, response, body) {
            if (!error && response.statusCode == 200) {
              const responseString = JSON.stringify(body);
              //เตรียม data และทำ query sql
              sql = "INSERT INTO log_noti(api_id,response,title,content) VALUES(?,?,?,?)";
              let titleStr = await TemplateDecode._decodeMessage(api.title_temp,body);
              let contentStr = await TemplateDecode._decodeMessage(api.content_temp,body);
              queryParams = [api.api_id, responseString,titleStr,contentStr];
              const db = new DBAPI(false);
              try {
                await db.query(sql, queryParams);
              } catch (except) {
                console.log(except);
              } finally {
                db.closeConnection(false);
              }
            }
          });
        }, api.api_interval * 1000));
      }
    } catch (error) {
      logger.info("getApiNotification() Exception : ", error);
    } finally {
      dbApi.closeConnection();
    }
  },
  updateApiNotification: async() => {
    if (apiRunningInterval.length !== apiStorage.length) {
      for (let i= apiRunningInterval.length-1;i<apiStorage.length;i++) {
		
        if (!apiRunningInterval.includes(apiStorage[i])) {
			apiRunningInterval.push(apiStorage[i]);
		  logger.info("API: { INTERVAL : ",apiStorage[i].api_interval,", ID:",apiStorage[i].api_id)
          apiIntervalId.push(setInterval(() => {
            request(apiStorage[i].api_key, async function (error, response, body) {
              if (!error && response.statusCode == 200) {
                const responseString = JSON.stringify(body);
                //เตรียม data และทำ query sql
                sql = "INSERT INTO log_noti(api_id,response,title,content) VALUES(?,?,?,?)";
                let titleStr = await TemplateDecode._decodeMessage(api.title_temp,responseString);
                let contentStr = await TemplateDecode._decodeMessage(api.content_temp,responseString);
                queryParams = [api.api_id, responseString,titleStr,contentStr];
                const db = new DBAPI(false);
                try {
                  await db.query(sql, queryParams);
                } catch (except) {
                  console.log(except);
                } finally {
                  db.closeConnection(false);
                }
              }
            });
          }, apiStorage[i].api_interval * 1000));
        }
      }
    }
  },

  clearApiInterval: (index = null)=>{
	  if(isNull(index)){
		for (const interval of apiIntervalId){
			clearInterval(interval);
		}
		apiRunningInterval = []
		apiIntervalId = []
	  }else{
		  clearInterval(apiIntervalId[index])
		  apiIntervalId.splice(index,1)
		  apiRunningInterval.splice(index,1)
	  }
	  
  },

};
