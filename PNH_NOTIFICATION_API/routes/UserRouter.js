const express = require("express");
const router = express.Router();

const path = require("path");
const UserEventRouter = require("./EventRouters/UserEventRouter");

router.get("/users", (req, res) => {
  UserEventRouter.listUser(req, res);
});

router.get("/user", (req, res) => {
  UserEventRouter.listUser(req, res);
});

router.post("/user", (req, res) => {
  UserEventRouter.addUser(req, res);
});

router.post("/user/login", (req, res) => {
  UserEventRouter.loginUser(req, res);
});

router.put("/user", (req, res) => {
  UserEventRouter.updateUser(req, res);
});

router.put("/user/status", (req, res) => {
  UserEventRouter.updateStatusUser(req, res);
});

router.delete("/user", (req, res) => {
  UserEventRouter.deleteUser(req, res);
});
module.exports = router;
