const HTTP = require("../../constants/http");
const MSG = require("../../constants/message");
const LogApiNotificationController = require("../../controllers/LogApiNotificationController");
const LoggerManager = require("../../utils/LoggerManager");
const logger = LoggerManager.getLogger("LogApiEventRouter");

class LogApiEventRouter {
    async listLog(req,res){
        try {
            logger.info("request ----> ",req.query);

            const successWithResult = await LogApiNotificationController.listLog(req.query);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("LogApiEventRouter.listLog() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async display(req,res){
        try {
            logger.info("request ----> ",req.query);

            const successWithResult = await LogApiNotificationController.display(req.query);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("LogApiEventRouter.listLog() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async delete(req,res){
        try {
            logger.info("request ----> ",req.query);

            const successWithResult = await LogApiNotificationController.deleteNotiApi(req.query);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("LogApiEventRouter.listLog() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    
}
module.exports = new LogApiEventRouter();