const HTTP = require("../../constants/http");
const MSG = require("../../constants/message");
const UserControllers = require('../../controllers/UserControllers');
const LoggerManager = require("../../utils/LoggerManager");
const logger = LoggerManager.getLogger("UserEventRouter");

class UserEventRouter {
    async listUser(req,res){
        try {
            logger.info("request ----> ",req.query);

            const successWithResult = await UserControllers.listUsers(req.query);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("UserEventRouter.listUser() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async addUser(req,res){
        try {
            logger.info("request ----> ",req.body);

            const successWithResult = await UserControllers.addUser(req.body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("UserEventRouter.addUser() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async updateUser(req,res){
        try {
            logger.info("request ----> ",req.body);

            const successWithResult = await UserControllers.updateUser(req.body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("UserEventRouter.updateUser() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async updateStatusUser(req,res){
        try {
            logger.info("request ----> ",req.body);

            const successWithResult = await UserControllers.updateStatusUser(req.body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("UserEventRouter.updateStatusUser() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async deleteUser(req,res){
        try {
            logger.info("request ----> ",req.query);

            const successWithResult = await UserControllers.deleteUser(req.query);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("UserEventRouter.deleteUser() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async loginUser(req,res){
        try {
            logger.info("request ----> ",req.body);

            const successWithResult = await UserControllers.loginUser(req.body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("UserEventRouter.loginUser() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }
}
module.exports = new UserEventRouter();