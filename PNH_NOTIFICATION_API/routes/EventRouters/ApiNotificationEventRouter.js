const HTTP = require("../../constants/http");
const MSG = require("../../constants/message");
const ApiNotificationController = require('../../controllers/ApiNotificationController');
const LoggerManager = require("../../utils/LoggerManager");
const logger = LoggerManager.getLogger("ApiNotificationEventRouter");
// ส่วนนี้จะเรียกข้อมูล request เพื่อส่งไป Controller และเมื่อได้รับข้อมูลจาก controller ก็จะส่งออกไปที่ response เพื่อแสดงผล

class ApiNotificationEventRouter {
    async listApi(req,res){
        try {
            logger.info("request ----> ",req.query);

            const successWithResult = await ApiNotificationController.listApi(req.query);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("ApiNotificationEventRouter.listApi() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async addApi(req,res){
        try {
            logger.info("request ----> ",req.body);

            const successWithResult = await ApiNotificationController.addApi(req.body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("ApiNotificationEventRouter.addApi() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async updateApi(req,res){
        try {
            logger.info("request ----> ",req.body);

            const successWithResult = await ApiNotificationController.updateApi(req.body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("ApiNotificationEventRouter.updateApi() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async updateStatusApi(req,res){
        try {
            logger.info("request ----> ",req.body);

            const successWithResult = await ApiNotificationController.updateStatusApi(req.body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("ApiNotificationEventRouter.updateStatusApi() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async deleteApi(req,res){
        try {
            logger.info("request ----> ",req.query);

            const successWithResult = await ApiNotificationController.deleteApi(req.query);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("ApiNotificationEventRouter.deleteApi() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }

    async listDetailApi(req,res){
        try {
            logger.info("request ----> ",req.body);

            const successWithResult = await ApiNotificationController.listDetailApi(req.body);
            res.status(HTTP.HTTP_SUCCESS_CODE).send(successWithResult);

        } catch (error) {
            logger.error("ApiNotificationEventRouter.listDetailApi() Exception : ",error);
            if(error.code != void 0){
                res.status(error.http).send({
                    statusCode: error.code,
                    statusMessage: error.data.message,
                });
            }else{
                res.status(HTTP.SYSTEM_ERR_HTTP_CODE).send({
                    statusCode: HTTP.SYSTEM_ERR_HTTP_CODE,
                    statusMessage: MSG.SYSTEM_ERROR_HTTP_MSG,
                });
            }
        }
    }
}
module.exports = new ApiNotificationEventRouter();