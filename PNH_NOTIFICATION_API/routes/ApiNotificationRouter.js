const express = require("express");
const router = express.Router();

const ApiNotificationEventRouter = require("./EventRouters/ApiNotificationEventRouter");

// part ชื่อของเส้น api
router.get("/api-notification", (req, res) => {
  ApiNotificationEventRouter.listApi(req, res);
});

router.post("/api-notification", (req, res) => {
  ApiNotificationEventRouter.addApi(req, res);
});

router.put("/api-notification", (req, res) => {
  ApiNotificationEventRouter.updateApi(req, res);
});

router.put("/api-notification/status", (req, res) => {
  ApiNotificationEventRouter.updateStatusApi(req, res);
});

router.delete("/api-notification", (req, res) => {
    ApiNotificationEventRouter.deleteApi(req, res);
});

router.post("/detail", (req, res) => {
  ApiNotificationEventRouter.listDetailApi(req, res);
});

module.exports = router;
