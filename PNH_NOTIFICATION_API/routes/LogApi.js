const express = require("express");
const router = express.Router();

const path = require("path");
const LogApiEventRouter = require("./EventRouters/LogApiEventRouter");

router.get("/log-api", (req, res) => {
  LogApiEventRouter.listLog(req, res);
});

router.get("/log-api/display", (req, res) => {
  LogApiEventRouter.display(req, res);
});

router.delete("/log-api", (req, res) => {
  LogApiEventRouter.delete(req, res);
});


module.exports = router;
