var mysql = require("mysql");

class DBAPI {
  #_con;
  constructor(isShow=true){
    this.getConnection()
    this.isShow = isShow
  }
  async getConnection() {
    try {
      this._con = mysql.createConnection({
        host: "localhost",
        user: "root",
        password: "",
        database: "pnh",
      });

      this._con.connect(function (err) {
        if (err) throw err;
        if(this.isShow)
          console.log("Connected!");
      });

      return this._con;
    } catch (error) {
      console.error("DBAPI EXCEPTION : ", error);
      throw error;
    }
  }

  async query(sql, params) {
    return await new Promise((resolve, reject) => {
      this._con.query(sql, params, (err, rows) => {
        if (err) reject(err);
        resolve(rows);
      });
    });
  }

  closeConnection() {
    if(this.isShow)
      console.log("Closed Connection");
    this._con.commit();
    this._con.end();
  }
}

module.exports = DBAPI;
